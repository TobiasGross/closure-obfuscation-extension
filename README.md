Required Tools
--------------
* Java 8
* Maven 3.3
* graphviz (for ast visualisation only)
* closure-compiler (for ast visualisation only)

Required Tools for testing
--------------------------
* NodeJS
* npm
* mocha
* Google Chrome Browser
```
npm install -g mocha
```


Build
-----
```
mvn package
```

Build and skip tests
-----
```
mvn package -DskipTests -Dmaven.test.skip=true
```

Build single package
--------------------
```
mvn package assembly:assembly
```

Generate Documentation
----------------------
```
mvn site
```
API doc is generated at "target/site/apidocs/index.html"

Run function renaming on test file
----------------------------------
```
cd target
java -jar closure-obfuscation-extension-1.0-SNAPSHOT.jar -O WHITESPACE_ONLY --js ../src/test/resources/nestedFunctions.js --rename_functions --formatting PRETTY_PRINT
```

Run print basic blocks on test file to generate graph picture
-------------------------------------------------------------
```
cd target
java -jar closure-obfuscation-extension-1.0-SNAPSHOT.jar --printBasicBlocks --js ../examples/printBBTest.js | dot -Tpng > "printBBTest.png"

```

Example:
![example basic block graph](https://gitlab.com/TobiasGross/closure-obfuscation-extension/raw/master/examples/printBBTest.png "example basic block graph")

Obfuscation Functionality switches
----------------------------------
```
--rename_functions
--flatten
--opaqueValues
--inlineFunctions
--mergeFunctions
--encryptStrings
--pack
--trap
```

Obfuscation level
-------------------
```
--low
--mediumA
--mediumB
--high
--max
```

Utility switches
----------------------------------
```
--printBasicBlocks
--printFunctionNames
```

Get ASTs of test files as PNG
-----------------------------
```
cd examples/graphs
./generateGraphs.sh
```

Performance Benchmarking
------------------------
To meassure the performance impact of the obfuscation methods, the bundle options are benchmarked. The suite is located in the benchmark folder.
The result of the benchmarks is located in the subdirectories bubble, bullet, chord, circle and tree for each d3 chart type. the naming convention is as follows:
{chartType}_{obfuscation_lvl}_{hostBrowser}.csv

To perform benchmarks on your own, you have to install node module "httpdispatcher" in the benchmark folder:
```
cd benchmark
npm install httpdispatcher
```
Then you have to start the server which receives the time measurements. the server takes 3 arguments:
```
node timingServer.js {chartType} {obfuscationLVL} {measurementCount}
```
The arguments chartType and obfuscationLVL are only used to name the output files. the measurementCount arg sets the amount of measurment iterations.
Once the server is started you can browse with a browser (Chrome, Firefox, Safari) to
```
http://localhost:1337/d3/{obfuscationLVL}/{chart}.html
```
obfuscation levels: src, low, mediumA, mediumB, high, max
charts: bubbleChart, bullet, chord, circle, radialTree
once the browser completed the rendering of the chart, the time measurement is communicated to the server and the page will automaticly relaod. This will happen till
the measurmentCount is reached. Multiple browser at the same time will work, but only call a chartType at once.