/**
    * Created by Tobias Gross on 26.09.16.
    */
function print(){
    var a = 0;
    a = a + 5;
    if(a < 3){
        console.log("3");
        a = a + 7;
    }else{
        console.log("7");
        a = a - 3;
        printTwo();
    }
}

function printTwo(){
    var a = 0;
    a = a + 5;
    if(a < 3){
        console.log("3");
        a = a + 7;
    }else{
        var b = 42;
        if(a < 5){
            console.log("7");
        }
        a = a - 3;
        function zz(a){
            var test = 42;
        }
    }
}

function wh(){
    var c = 0;
    while(c < 5){
        c++;
        console.log(c);
        for(var i = 0; i < 10; i++){
            printTwo();
            for(var d = 0; d < 10; d++){
                printTwo();
                for(var e = 0; e < 10; e++){
                    printTwo();
                }
            }
        }
    }
    return c;
}

function breakTest(){
    var i = 0;
    while(true){
        if(i > 5){
            break;
            i = 10;
        }
        i++;
        if(i < 3){
            continue;
        }

        i = 20;
    }
}

function switchTest(){
    var a = 0;
    switch(a){
        case 0:
            a = 2;
            break;
        case 1:
            a = 3;
        case 2:
            a = 5;
            break;
        default:
            a = 7;
    }
}

function forTest(){
    for(var a = 0; a < 10; a++){
        breakTest();
    }
}