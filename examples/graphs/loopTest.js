/**
 * Created by Tobias Gross on 17.09.16.
 */

function loopTest(){
    for(var i = 0; i < 5; i++){
        for(var n = 0; n < 5; n++){
            console.log(i + n);
        }
    }
    for(; i > 4; i++);
}
loopTest();