#@IgnoreInspection BashAddShebang
rm *.png
for f in *.js
do
    closure-compiler --js $f -O WHITESPACE_ONLY --print_ast | dot -Tpng > "$f.png" &
done
wait