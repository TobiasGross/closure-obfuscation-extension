/**
 * Created by Tobias Gross on 21.09.16.
 */


/**
 * @inline
 */
function bla(){
    var a = 5;
    if(a == 5){
        var a = 7;
        if(a = 7){
            var a = 9;
            console.log(a);
        }
        console.log(a);
    }
    console.log(a);
}

function fu(){
    var b = 3;
    for(var i = 0; i < 1; i++){
        var c = 5;
        console.log(c);
    }
    console.log(b);
}

function basa(){
    var bc = 0;
    while(true){
        switch(bc){
            case 0:
                var a = 3;
                console.log(a);
                bc = 1;
                break;
            case 1:
                var a = 5;
                console.log(a);
                bc = 2;
                break;
            default:
                var a = 7;
            case 2:
                console.log(a);
                return;
        }
    }
}

bla();
fu();
basa();