/**
 * Created by grs on 16.10.16.
 */
function fu(properties){
    for (var propertyName in properties) {
        if (properties.hasOwnProperty(propertyName)) {
            this[propertyName] = properties[propertyName];
        }
    }
}