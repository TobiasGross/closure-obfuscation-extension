/**
 * Created by grs on 30.12.16.
 */
function sendTiming(timing){
    var http = new XMLHttpRequest();
    var url = "/timing";
    var params = "timeDelta=" + timing;
    http.open("POST", url, true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.onreadystatechange = function() {//Call a function when the state changes.
        if(http.readyState == 4 && http.status == 200) {
            if(http.responseText === "true"){
                location.reload(true);
            }
        }
    }
    http.send(params);
}

window.addEventListener("load",  function(){
    document.getElementById("startTime").innerHTML = "Connect Start: " + performance.timing.connectStart;
});