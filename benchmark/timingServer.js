/**
 * Created by grs on 30.12.16.
 */
var http = require('http');
var HttpDispatcher = require('httpdispatcher');
var dispatcher     = new HttpDispatcher();
var fs = require('fs');

const PORT = 1337

var chartType = process.argv[2];
var obfuscation = process.argv[3];
var times = parseInt(process.argv[4]);


var chromeCount = times;
var firefoxCount = times;
var safariCount = times;

var chromeFD = openFile(chartType + "_" + obfuscation + "_chrome.csv");
var firefoxFD = openFile(chartType + "_" + obfuscation + "_firefox.csv");
var safariFD = openFile(chartType + "_" + obfuscation + "_safari.csv");

function openFile(name) {
    var myTimingFile;
    fs.open(name, 'a', function (err, fd) {
        if (err) {
            throw 'error opening file: ' + err;
        }
        myTimingFile = fd;
    });

    function writeTimingVal(value) {
        var buffer = new Buffer(value + "\n");
        fs.write(myTimingFile, buffer, 0, buffer.length, null, function (err) {
            if (err) throw 'error writing file: ' + err;
        });
    }
    return writeTimingVal;
}


function handleRequest(request, response){
    try{
        dispatcher.dispatch(request, response);
    }catch(err){
        console.log(err);
    }
}

dispatcher.setStatic('/d3');
dispatcher.setStaticDirname('d3');
dispatcher.onPost("/timing", function(request, response){
    var agent = request.headers["user-agent"];
    var timeDelta = request.params.timeDelta;
    var thatCount;
    var logPrefix;

    if(agent.includes("Chrome")){
        chromeCount--;
        thatCount = chromeCount;
        logPrefix = "C ";
        chromeFD(timeDelta);

    }else if(agent.includes("Firefox")){
        firefoxCount--;
        thatCount = firefoxCount;
        logPrefix = "F ";
        firefoxFD(timeDelta);
    }else{
        safariCount--;
        thatCount = safariCount;
        logPrefix = "S ";
        safariFD(timeDelta);
    }
    console.log(logPrefix + timeDelta);

    if(thatCount > 0){
        response.end("true");
    }else{
        response.end("false");
    }
});

var server = http.createServer(handleRequest);

server.listen(PORT, function(){
   console.log("Server listening on http://localhost:%s", PORT);
});