/**
 * Created by grs on 27.12.16.
 */
function a(paramOne, paramTwo){
    var retval = 0;
    one:{
        if(paramOne){
            retval = 1;
            break one;
        }
        retval = 2;

    }
    two:{
        if(paramTwo){
            retval = 3;
            break two;
        }
    }
    return retval;
}

function testOne(){
    return a(true, false);
}

function testTwo(){
    return a(false, false);
}

function testThree(){
    return a(true, true);
}

module['exports']['one'] = testOne;
module['exports']['two'] = testTwo;
module['exports']['three'] = testThree;