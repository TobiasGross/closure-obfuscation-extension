/**
 * Created by Tobias Gross on 31.07.16.
 */
function func(){
    return "func";
}
var funcOne = func;
var funcTwo;
funcTwo = func;

module['exports']['funcOne'] = funcOne;
module['exports']['funcTwo'] = funcTwo;