/**
 * Created by Tobias Gross on 01.08.16.
 */


function one(){
    return 1;
}
function two(){
    return 2;
}
function caller(one, two){
    return one() / two();
}

function process(){
    return caller(two, one);
}

module['exports']['process'] = process;