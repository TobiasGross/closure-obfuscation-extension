/**
 * Created by grs on 19.10.16.
 */

function call(){
    try{
        var a = true;
        if(a){
            helper(true);
        }else
            helper(false);
    }catch(e){
        return e;
    }
}

function helper(h){
    if(h){
        throw 42;
    }else{
        throw 1337;
    }
}

module['exports']['call'] = call;