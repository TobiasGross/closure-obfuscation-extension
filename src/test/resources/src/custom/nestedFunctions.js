/**
 * Created by Tobias Gross on 31.07.16.
 */


function outer(){
    var sum = 0;
    function inner(){
        return 2 + outerTwo();
    }
    function innerTwo(){
        return 3 + inner();
    }
    sum += innerTwo();

    sum += outerTwo();

    function outerTwo(){
        return 1;
    }
    sum += outerTwo();
    return sum;
}

function outerTwo(){
    return 42;
}

module['exports']['outer'] = outer;
module['exports']['outerTwo'] = outerTwo;