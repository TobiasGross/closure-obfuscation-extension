/**
 * Created by Tobias Gross on 31.07.16.
 */

var funcOne = function(){
    return 1;
}

function funcTwo(){
    return 2;
}

var mapWrapper = {
    one:funcOne,
    two:funcTwo
};

function thisTest(){
    var _this = this;
    _this.one = funcOne;
    _this.two = funcTwo;
    return _this;
}

module['exports']['funcOne'] = funcOne;
module['exports']['funcTwo'] = funcTwo;

module['exports']['mapWrapper'] = mapWrapper;
module['exports']['thisTest'] = thisTest;