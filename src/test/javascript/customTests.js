/**
 * Created by Tobias Gross on 16.09.16.
 */
var assert = require('assert');

var srcBase = '../resources/src/custom/';

var compilerConfigs = [
    {
        "file": "../resources/build/renameFlattening/custom/",
        "description": "code flattening and identifier renaming"
    },
    {
        "file": "../resources/build/functionRenaming/custom/",
        "description": "function renaming only"
    },
    {
        "file": "../resources/build/codeFlattening/custom/",
        "description": "code flattening only"
    },
    {
        "file": "../resources/build/flatteningOpaque/custom/",
        "description": "code flattening and opaque values"
    },
    {
        "file": "../resources/build/functionInlining/custom/",
        "description": "function inlining only"
    },
    {
        "file": "../resources/build/functionMerging/custom/",
        "description": "function merging only"
    },
    {
        "file": "../resources/build/inliningMerging/custom/",
        "description": "function merging and inlining"
    },
    {
        "file": "../resources/build/stringEncryption/custom/",
        "description": "string encryption only"
    },
    {
        "file": "../resources/build/low/custom/",
        "description": "low level obfuscation"
    },
    {
        "file": "../resources/build/mediumA/custom/",
        "description": "medium level obfuscation"
    },
    {
        "file": "../resources/build/mediumB/custom/",
        "description": "medium level obfuscation"
    },
    {
        "file": "../resources/build/high/custom/",
        "description": "high level obfuscation"
    },
    {
        "file": "../resources/build/max/custom/",
        "description": "max level obfuscation"
    },
    {
        "file": "../resources/build/mergeInlineFlattenQpaque/custom/",
        "description": "function merging and inlining. code flattening and opaque values"
    }
];

describe('Custom', function () {
    compilerConfigs.forEach(function (compilerConifg) {
        describe(compilerConifg.description, function () {
            var funcDefOneSrc = require(srcBase + 'funcDefOne.js');
            var funcDefOneBuild = require(compilerConifg.file + 'funcDefOne.js');

            var funcRefAsParamSrc = require(srcBase + 'funcRefAsParam.js');
            var funcRefAsParamBuild = require(compilerConifg.file + 'funcRefAsParam.js');

            var functionDeclSrc = require(srcBase + 'functionDecl.js');
            var functionDeclBuild = require(compilerConifg.file + 'functionDecl.js');

            var nestedFunctionsSrc = require(srcBase + 'nestedFunctions.js');
            var nestedFunctionsBuild = require(compilerConifg.file + 'nestedFunctions.js');

            var tryCatchSrc = require(srcBase + 'tryCatch.js');
            var tryCatchBuild = require(compilerConifg.file + 'tryCatch.js');

            var labeledBlockSrc = require(srcBase + 'labeledBlock.js');
            var labeledBlockBuild = require(compilerConifg.file + 'labeledBlock.js');

            it('#funcDefOne', function () {
                var valueOne_src = funcDefOneSrc.funcOne();
                var valueTwo_src = funcDefOneSrc.funcTwo();

                var valueOne_dst = funcDefOneBuild.funcOne();
                var valueTwo_dst = funcDefOneBuild.funcTwo();

                assert.equal(valueOne_src, valueTwo_src);
                assert.equal(valueOne_dst, valueTwo_dst);
                assert.equal(valueOne_src, valueOne_dst);
                assert.equal(valueTwo_src, valueTwo_dst);
            });

            it('#funcRefAsParam', function () {
                var valueOne_src = funcRefAsParamSrc.process();
                var valueOne_dst = funcRefAsParamBuild.process();

                assert.equal(valueOne_src, valueOne_dst);
            });

            it('#functionDecl', function () {
                assert.equal(functionDeclSrc.funcOne(), functionDeclBuild.funcOne());
                assert.equal(functionDeclSrc.funcTwo(), functionDeclBuild.funcTwo());
                assert.equal(functionDeclSrc.mapWrapper.one(), functionDeclBuild.mapWrapper.one());
                assert.equal(functionDeclSrc.mapWrapper.two(), functionDeclBuild.mapWrapper.two());

                var srcThis = functionDeclSrc.thisTest();
                var dstThis = functionDeclBuild.thisTest();
                assert.equal(srcThis.one(), dstThis.one());
                assert.equal(srcThis.two(), dstThis.two());
            });

            it('#nestedFunction', function () {
                assert.equal(nestedFunctionsSrc.outer(), nestedFunctionsBuild.outer());
                assert.equal(nestedFunctionsSrc.outerTwo(), nestedFunctionsBuild.outerTwo());
            });

            it('#tryCatch', function () {
                assert.equal(tryCatchSrc.call(), tryCatchBuild.call());
            });

            it('#labledBlock', function () {
                assert.equal(labeledBlockSrc.one(), labeledBlockBuild.one());
                assert.equal(labeledBlockSrc.two(), labeledBlockBuild.two());
                assert.equal(labeledBlockSrc.three(), labeledBlockBuild.three());
            });
        });
    });
});
