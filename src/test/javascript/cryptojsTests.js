/**
 * Created by Tobias Gross on 05.08.16.
 */
var assert = require('assert');

var compilerConfigs = [{
    "file": "../resources/build/functionRenaming/cryptojs",
    "description": "identifier renaming only"
},
    {
        "file": "../resources/build/renameFlattening/cryptojs",
        "description": "code flattening and identifier renaming"
    },
    {
        "file": "../resources/build/codeFlattening/cryptojs",
        "description": "code flattening only"
    },
    {
        "file": "../resources/build/flatteningOpaque/cryptojs",
        "description": "code flattening and opaque values"
    },
    {
        "file": "../resources/build/functionInlining/cryptojs",
        "description": "function inlining only"
    },
    {
        "file": "../resources/build/functionMerging/cryptojs",
        "description": "function merging only"
    },
    {
        "file": "../resources/build/inliningMerging/cryptojs",
        "description": "function merging and inlining"
    },
    {
        "file": "../resources/build/stringEncryption/cryptojs",
        "description": "string encryption only"
    },
    {
        "file": "../resources/build/low/cryptojs",
        "description": "low level obfuscation"
    },
    {
        "file": "../resources/build/mediumA/cryptojs",
        "description": "mediumA level obfuscation"
    },
    {
        "file": "../resources/build/mediumB/cryptojs",
        "description": "mediumB level obfuscation"
    },
    {
        "file": "../resources/build/high/cryptojs",
        "description": "high level obfuscation"
    },
    {
        "file": "../resources/build/max/cryptojs",
        "description": "max level obfuscation"
    },
    {
        "file": "../resources/build/mergeInlineFlattenQpaque/cryptojs",
        "description": "function merging and inlining. code flattening and opaque values"
    }
];

var CryptoJS = require('../resources/src/cryptojs');

var clearText = 'my message';
var secretKey = 'secret key 123';
var salt = CryptoJS.lib.WordArray.random(128 / 8);


describe('CryptoJS', function () {
    compilerConfigs.forEach(function (config) {
        describe(config.description, function () {
            var cryptoJS_build = require(config.file);

            var cryptoTestCases = ['MD5',
                'SHA1',
                'SHA224',
                'SHA256',
                'SHA384',
                'SHA512',
                'RIPEMD160'
            ];
            cryptoTestCases.forEach(function (funcName) {
                it('#' + funcName, function (done) {
                    var hash_src = CryptoJS[funcName](clearText);
                    var hash_dst = cryptoJS_build[funcName](clearText);
                    assert.equal(hash_src.toString(), hash_dst.toString());
                    done();
                });
            });

            var outputLength = [224, 256, 384, 512];
            outputLength.forEach(function (length) {
                it('#SHA3 - ' + length, function () {
                    var hash_src = CryptoJS.SHA3(clearText, {outputLength: length});
                    var hash_dst = cryptoJS_build.SHA3(clearText, {outputLength: length});
                    assert.equal(hash_src.toString(), hash_dst.toString());
                });
            });

            var hmacFunctions = ['HmacMD5',
                'HmacSHA1',
                'HmacSHA256',
                'HmacSHA512'
            ];
            hmacFunctions.forEach(function (funcName) {
                it('#' + funcName, function () {
                    var hmac_src = CryptoJS[funcName](clearText, secretKey);
                    var hmac_dst = cryptoJS_build[funcName](clearText, secretKey);
                    assert.equal(hmac_src.toString(), hmac_dst.toString());
                });
            });

            var keyConfigs = [{keySize: 128 / 32}, {keySize: 256 / 32}, {keySize: 512 / 32}, {
                keySize: 512 / 32,
                iterations: 20
            }];
            keyConfigs.forEach(function (keyconfig) {
                it('#PBKDF2 - ' + keyconfig.keySize, function () {
                    var key_src = CryptoJS.PBKDF2(clearText, salt, keyconfig);
                    var key_dst = cryptoJS_build.PBKDF2(clearText, salt, keyconfig);
                    assert.equal(key_src.toString(), key_dst.toString());
                });
            });

            var casesTwo = ['AES', 'DES', 'TripleDES', 'Rabbit', 'RC4', 'RC4Drop'];
            casesTwo.forEach(function (caze) {
                it('#' + caze, function () {
                    var enc_src = CryptoJS[caze].encrypt(clearText, secretKey);
                    var enc_dst = cryptoJS_build[caze].encrypt(clearText, secretKey);

                    var dec_src = CryptoJS[caze].decrypt(enc_src.toString(), secretKey);
                    var dec_dst = cryptoJS_build[caze].decrypt(enc_dst.toString(), secretKey);


                    assert.equal(enc_src.ciphertext.toString().length, enc_dst.ciphertext.toString().length);
                    assert.equal(dec_src.toString(CryptoJS.enc.Utf8), dec_dst.toString(CryptoJS.enc.Utf8));
                });
            });
        });
    });
});
