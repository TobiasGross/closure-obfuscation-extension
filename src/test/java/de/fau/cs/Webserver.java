package de.fau.cs;

import fi.iki.elonen.NanoHTTPD;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * a small http server which serves all files for selenium tests.
 * @author Tobias Gross
 */
public class Webserver extends NanoHTTPD{
    public final static int TEST_HTTP_PORT = 8080;
    private final static String PATH_TO_TEST_RESOURCES = "/src/test/resources";
    private final String workingDir;

    public Webserver() throws IOException{
        super(TEST_HTTP_PORT);
        workingDir = System.getProperty("user.dir");
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
    }

    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();
        try {
            String requestedFile = workingDir + PATH_TO_TEST_RESOURCES + uri;
            File file = new File(requestedFile);
            if(!file.exists()){
                return newFixedLengthResponse(Response.Status.NOT_FOUND, "", "");
            }
            FileInputStream stream = FileUtils.openInputStream(file);
            String answer = IOUtils.toString(stream);
            return newFixedLengthResponse(answer);

        } catch (IOException e) {
            e.printStackTrace();
            return newFixedLengthResponse("Exception");
        }
    }

    public static void main(String[] args){
        try {
            Webserver w = new Webserver();
            System.out.println("server started");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Main end");
    }

    public String getWebRoot(){
        return workingDir + PATH_TO_TEST_RESOURCES;
    }
}
