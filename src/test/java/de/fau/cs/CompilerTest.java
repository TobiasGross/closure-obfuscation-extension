package de.fau.cs;

import com.google.javascript.jscomp.AbstractCommandLineRunner;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This test class compiles several JavaScript files with different compiler options.
 * Later, return values from functions in original JavaScript files are comapered to the compiled functions return values.
 * @author Tobias Gross
 */
public class CompilerTest {
    private final static String PATH_TO_SRC = "/src/test/resources/src";
    private final static String PATH_TO_TARGET = "/src/test/resources/build";

    private final static String FUNCTION_RENAMING_PATH = "/functionRenaming";
    private final static String[] ARGS = {"--js", "src", "--js_output_file", "build","-O", "WHITESPACE_ONLY", "--formatting", "PRETTY_PRINT"};
    private final static String[] CUSTOM_ARGS_FUNCTION_RENAMING = {"--rename_functions"};

    private final static String FUNCTION_INLINING_PATH = "/functionInlining";
    private final static String[] CUSTOM_ARGS_FUNCTION_INLINING = {"--inlineFunctions"};

    private final static String FUNCTION_MERGING_PATH = "/functionMerging";
    private final static String[] CUSTOM_ARGS_FUNCTION_MERGING = {"--mergeFunctions"};

    private final static String Inlining_MERGING_PATH = "/inliningMerging";
    private final static String[] CUSTOM_ARGS_Inlining_MERGING = {"--mergeFunctions", "--inlineFunctions"};

    private final static String SIMPLE_PATH = "/simple";
    private final static String[] ARGS_SIMPLE_LVL = {"--js", "src", "--js_output_file", "build","-O", "SIMPLE", "--formatting", "PRETTY_PRINT"};

    private final static String CODE_FLATTENING_PATH = "/codeFlattening";
    private final static String[] CUSTOM_ARGS_CODE_FLATTENING = {"--flatten"};

    private final static String FLATTENING_OPAQUE_PATH = "/flatteningOpaque";
    private final static String[] CUSTOM_ARGS_FLATTENING_OPAQUE = {"--flatten", "--opaqueValues"};

    private final static String RENAME_FLATTENING_PATH = "/renameFlattening";
    private final static String[] CUSTOM_ARGS_RENAME_FLATTENING = {"--flatten", "--rename_functions"};

    private final static String STRING_ENCRYPTION_PATH = "/stringEncryption";
    private final static String[] CUSTOM_ARGS_STRING_ENCRYPTION = {"--encryptStrings"};

    private final static String LOW_PATH = "/low";
    private final static String[] CUSTOM_ARGS_LOW = {"--low"};

    private final static String MEDIUM_A_PATH = "/mediumA";
    private final static String[] CUSTOM_ARGS_MEDIUM_A = {"--mediumA"};

    private final static String MEDIUM_B_PATH = "/mediumB";
    private final static String[] CUSTOM_ARGS_MEDIUM_B = {"--mediumB"};

    private final static String HIGH_PATH = "/high";
    private final static String[] CUSTOM_ARGS_HIGH = {"--high"};

    private final static String MAX_PATH = "/max";
    private final static String[] CUSTOM_ARGS_MAX = {"--max"};

    private final static String MERGE_INLINE_FLATTEN_OPAQUE_PATH = "/mergeInlineFlattenQpaque";
    private final static String[] CUSTOM_ARGS_MERGE_INLINE_FLATTEN_OPAQUE = {"--inlineFunctions", "--mergeFunctions", "--flatten", "--opaqueValues"};

    private final static String MERGE_INLINE_FLATTEN_PATH = "/mergeInlineFlatten";
    private final static String[] CUSTOM_ARGS_MERGE_INLINE_OPAQUE = {"--inlineFunctions", "--mergeFunctions", "--flatten"};

    private final static int SRC_INDEX = 1;
    private final static int DST_INDEX = 3;

    /**
     *
     * @return absolute path to JavaScript source files root directory
     */
    private static String getPathToSrc(){
        return System.getProperty("user.dir") + PATH_TO_SRC;
    }

    /**
     *
     * @return absolute path to compiled JavaScript files root directory
     */
    private static String getPathToTarget(){
        return System.getProperty("user.dir") + PATH_TO_TARGET;
    }

    /**
     * removes all visible files in a given directory, if given file is a directory. Removes given file otherwise.
     * @param target points to a file or directory
     */
    private static void removeFiles(File target){
        for(File file : target.listFiles()){
            if(file.isDirectory()){
                removeFiles(file);
            }else if (file.getName().endsWith(".js")){
                file.delete();
            }
        }
    }

    /**
     * Searches recursively through a folder and collects all *.js files
     * @param folder
     * @return all JavaScript files in a given directory
     */
    private List<File> getFilesToProcess(File folder){
        List<File> files = new ArrayList<>();
        for(File file : folder.listFiles()){
            if(file.isDirectory()){
                files.addAll(getFilesToProcess(file));
            }else if (file.getName().endsWith(".js")){
                files.add(file);
            }
        }
        return files;
    }

    /**
     * Builds the target path for a given source file.
     * @param src Original file
     * @param pathModifier part of the path, which describes the compiler run configuration
     * @return
     */
    private String getTargetPath(File src, String pathModifier){
        String regEx = getPathToSrc().replace(File.separator, "\\" + File.separator);
        regEx = regEx + "(.*)";
        Pattern pattern = Pattern.compile(regEx);
        try {
            Matcher matcher = pattern.matcher(src.getCanonicalPath());
            if(matcher.find()){
                return getPathToTarget() + pathModifier + matcher.group(1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method fake the AbstractCommandLineRunner.run() method. The original will call System.exit which is here misplaced, because this will end the test framework.
     * To fake the run method, the protected method doRun have to set accessible. Instead of exiting, the return code is returned by this method.
     * @param cliRunner
     * @return
     */
    private int compilerRunMethodForTesting(CLIRunner cliRunner){
        try {
            Method doRunMethod = cliRunner.getClass().getSuperclass().getSuperclass().getDeclaredMethod("doRun");
            doRunMethod.setAccessible(true);
            int result = 0;
            int runs = 1;
            try {
                for (int i = 0; i < runs && result == 0; i++) {
                    Object retVal = doRunMethod.invoke(cliRunner);
                    result = (Integer)retVal;
                }
            } catch (AbstractCommandLineRunner.FlagUsageException e) {
                System.err.println(e.getMessage());
                result = -1;
            } catch (Throwable t) {
                t.printStackTrace();
                result = -2;
            }
            return result;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return -3;
        }
    }

    /**
     * Removes old compiled JavaScript files.
     */
    @BeforeClass
    public static void prepare(){
        removeFiles(new File(getPathToTarget()));
    }

    /**
     * Takes a compiler configuration and call compiler.
     * @param pathModifier describes the compiler configuration
     * @param args arguments for stock closure compiler
     * @param customArgs arguments for obfuscation extension
     */
    private void compileAllFiles(String pathModifier, String[] args, List<String> customArgs){
        for(File f : getFilesToProcess(new File(getPathToSrc()))){
            try {
                String src = f.getCanonicalPath();
                String dst = getTargetPath(f, pathModifier);
                args[SRC_INDEX] = src;
                args[DST_INDEX] = dst;

                CLIRunner runner = new CLIRunner(args, customArgs, 1);
                Assert.assertTrue("Compiler should run", runner.shouldRunCompiler());
                int compilerStatus = compilerRunMethodForTesting(runner);
                Assert.assertFalse("Compiler exit code " + compilerStatus, compilerStatus < 0);
            } catch (IOException e) {
                e.printStackTrace();
                Assert.fail("Could not get Path for File");
            }
        }
    }

    /**
     * Runs compiler on source JS files with following configuration:
     * --js somePath --js_output_file somePath -O SIMPLE --formatting PRETTY_PRINT
     */
//    @Test
    public void simpleOnly() {
        compileAllFiles(SIMPLE_PATH, ARGS_SIMPLE_LVL, new ArrayList<>());
    }

    /**
     * Runs compiler on source JS files with following configuration:
     * --js somePath --js_output_file somePath -O WHITESPACE_ONLY --formatting PRETTY_PRINT --rename_functions
     */
    @Test
    public void functionRenamingOnly() {
        compileAllFiles(FUNCTION_RENAMING_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_FUNCTION_RENAMING));
    }

    /**
     * Runs compiler on source JS files with following configuration:
     * --js somePath --js_output_file somePath -O WHITESPACE_ONLY --formatting PRETTY_PRINT --flatten
     */
    @Test
    public void codeFlatteningOnly(){
        compileAllFiles(CODE_FLATTENING_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_CODE_FLATTENING));
    }

    @Test
    public void renamingFlattening(){
        compileAllFiles(RENAME_FLATTENING_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_RENAME_FLATTENING));
    }

    @Test
    public void opaqueValuesFlattening(){
        compileAllFiles(FLATTENING_OPAQUE_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_FLATTENING_OPAQUE));
    }

    @Test
    public void functionInliningOnly(){
        compileAllFiles(FUNCTION_INLINING_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_FUNCTION_INLINING));
    }

    @Test
    public void functionMergingOnly(){
        compileAllFiles(FUNCTION_MERGING_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_FUNCTION_MERGING));
    }

    @Test
    public void inlingingMerging(){
        compileAllFiles(Inlining_MERGING_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_Inlining_MERGING));
    }

    @Test
    public void stringEncryption(){
        compileAllFiles(STRING_ENCRYPTION_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_STRING_ENCRYPTION));
    }

    @Test
    public void low(){
        compileAllFiles(LOW_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_LOW));
    }

    @Test
    public void mediumA(){
        compileAllFiles(MEDIUM_A_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_MEDIUM_A));
    }

    @Test
    public void mediumB(){
        compileAllFiles(MEDIUM_B_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_MEDIUM_B));
    }

    @Test
    public void high(){
        compileAllFiles(HIGH_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_HIGH));
    }

    @Test
    public void max(){
        compileAllFiles(MAX_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_MAX));
    }

    @Test
    public void mergeInlineFlattenOpaque(){
        compileAllFiles(MERGE_INLINE_FLATTEN_OPAQUE_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_MERGE_INLINE_FLATTEN_OPAQUE));
    }

    @Test
    public void mergeInlineFlatten(){
        compileAllFiles(MERGE_INLINE_FLATTEN_PATH, ARGS, Arrays.asList(CUSTOM_ARGS_MERGE_INLINE_OPAQUE));
    }
}
