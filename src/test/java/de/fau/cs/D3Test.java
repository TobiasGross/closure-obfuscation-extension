package de.fau.cs;

import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * This class tests the obfuscation transformations on the d3 framework. d3 is a framework for data visualization in browsers.
 * The tests are performed with selenium test framework. this framework utilizes a browser to perform gui interactions.
 * In this case it calls a html sites which displays diagrams. for every test two sites are opened. one with source js files
 * and another with obfuscated js files. the content is compared via screenshot data. if both site calls display the same
 * content the test is passed. if the content differ the test fails.
 *
 * @author Tobias Gross
 */
public class D3Test {

    private final static String PATH_CHROME_OSX = "/lib/chrome/osx/chromedriver";
    private final static String PATH_CHROME_WIN = "/lib/chrome/win/chromedriver";
    private final static String PATH_CHROME_LINUX = "/lib/chrome/linux/chromedriver";

    private final static int WAIT_TIME_SCREENSHOT = 1500;

    private static Webserver webserver;
    private static ChromeDriver chromeDriver;
    private static HashMap<String, String> screenCache;

    /**
     * Initializes webserver which serves web content.
     * @throws IOException
     */
    @BeforeClass
    public static void prepareTests() throws IOException {
        configGecko();
        webserver = new Webserver();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        chromeDriver = new ChromeDriver(options);
        screenCache = new HashMap<>();
    }

    /**
     * this method sets chrome testdriver dependent on operating system
     */
    private static void configGecko() {
        String OS = System.getProperty("os.name").toLowerCase();
        String cwd = System.getProperty("user.dir");
        String chromePath;

        if (OS.indexOf("mac") >= 0) {
            chromePath = cwd + PATH_CHROME_OSX;
        } else if (OS.indexOf("win") >= 0) {
            chromePath = cwd + PATH_CHROME_WIN;
        } else {
            chromePath = cwd + PATH_CHROME_LINUX;
        }
        System.setProperty("webdriver.chrome.driver", chromePath);
    }


    /**
     * stops the webserver.
     */
    @AfterClass
    public static void tearDown() {
        webserver.stop();
        chromeDriver.quit();
    }

    private void performD3TestChrome(String srcUrl, String buildUrl) throws InterruptedException {
        String srcScreenBase64;
        if(screenCache.containsKey(srcUrl)){
            srcScreenBase64 = screenCache.get(srcUrl);
        }else{
            chromeDriver.get("localhost:" + Webserver.TEST_HTTP_PORT + srcUrl);
            Thread.sleep(WAIT_TIME_SCREENSHOT);
            srcScreenBase64 = chromeDriver.getScreenshotAs(OutputType.BASE64);
            screenCache.put(srcUrl, srcScreenBase64);
        }

        chromeDriver.get("localhost:" + Webserver.TEST_HTTP_PORT + buildUrl);
        Thread.sleep(WAIT_TIME_SCREENSHOT);
        File buildScreen = chromeDriver.getScreenshotAs(OutputType.FILE);
        String buildScreenBase64 = chromeDriver.getScreenshotAs(OutputType.BASE64);

        try {
            FileUtils.copyFile(buildScreen, new File(webserver.getWebRoot() + buildUrl + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("Screenshots should be equal", srcScreenBase64, buildScreenBase64);
    }

    /**
     * this method tests function renaming on a d3 bubble chart display
     */
    @Test
    public void bubbleChartFunctionRenaming() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/functionRenaming/d3/bubbleChart.html");
    }

    @Test
    public void bubbleChartCodeFlatten() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/codeFlattening/d3/bubbleChart.html");
    }

    @Test
    public void bubbleChartRenamingFlatten() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/renameFlattening/d3/bubbleChart.html");
    }

    @Test
    public void bubbleChartFlattenOpaque() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/flatteningOpaque/d3/bubbleChart.html");
    }

    @Test
    public void bubbleChartFunctionInlining() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/functionInlining/d3/bubbleChart.html");
    }

    @Test
    public void bubbleChartFunctionMerging() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/functionMerging/d3/bubbleChart.html");
    }

    @Test
    public void bubbleChartInliningMerging() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/inliningMerging/d3/bubbleChart.html");
    }

    @Test
    public void bubbleChartStringEncryption() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/stringEncryption/d3/bubbleChart.html");
    }

    @Test
    public void bubbleChartLow() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/low/d3/bubbleChart.html");
    }

    @Test
    public void bubbleChartMedium() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/medium/d3/bubbleChart.html");
    }

    @Test
    public void bubbleChartHigh() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/high/d3/bubbleChart.html");
    }

    @Test
    public void bubbleChartMergeInlineFlattenQpaque() throws InterruptedException {
        performD3TestChrome("/src/d3/bubbleChart.html", "/build/mergeInlineFlattenQpaque/d3/bubbleChart.html");
    }

    //======================================================================================================

    @Test
    public void chordChartCodeFlatten() throws InterruptedException {
        performD3TestChrome("/src/d3/chord.html", "/build/codeFlattening/d3/chord.html");
    }

    @Test
    public void chordChartFunctionRenaming() throws InterruptedException {
        performD3TestChrome("/src/d3/chord.html", "/build/functionRenaming/d3/chord.html");
    }

    @Test
    public void chordChartRenamingFlatten() throws InterruptedException {
        performD3TestChrome("/src/d3/chord.html", "/build/renameFlattening/d3/chord.html");
    }

    @Test
    public void chordChartFunctionInlining() throws InterruptedException {
        performD3TestChrome("/src/d3/chord.html", "/build/functionInlining/d3/chord.html");
    }

    @Test
    public void chordChartFunctionMerging() throws InterruptedException {
        performD3TestChrome("/src/d3/chord.html", "/build/functionMerging/d3/chord.html");
    }

    @Test
    public void chordChartInliningMerging() throws InterruptedException {
        performD3TestChrome("/src/d3/chord.html", "/build/inliningMerging/d3/chord.html");
    }

    @Test
    public void chordChartStringEncryption() throws InterruptedException {
        performD3TestChrome("/src/d3/chord.html", "/build/stringEncryption/d3/chord.html");
    }

    @Test
    public void chordChartLow() throws InterruptedException {
        performD3TestChrome("/src/d3/chord.html", "/build/low/d3/chord.html");
    }

    @Test
    public void chordChartMedium() throws InterruptedException {
        performD3TestChrome("/src/d3/chord.html", "/build/medium/d3/chord.html");
    }

    @Test
    public void chordChartHigh() throws InterruptedException {
        performD3TestChrome("/src/d3/chord.html", "/build/high/d3/chord.html");
    }

    @Test
    public void chordChartMergeInlineFlattenQpaque() throws InterruptedException {
        performD3TestChrome("/src/d3/chord.html", "/build/mergeInlineFlattenQpaque/d3/chord.html");
    }

//    @Test
//    public void chordChartFlattenOpaque() throws InterruptedException {
//        performD3TestChrome("/src/d3/bubbleChart.html", "/build/flatteningOpaque/d3/chord.html");
//    }
    //======================================================================================================

    //automatic test fails with no reason :(
//    @Test
//    public void bulletChartCodeFlatten() throws InterruptedException {
//        performD3TestChrome("/src/d3/chord.html", "/build/codeFlattening/d3/bullet.html");
//    }
//
//    @Test
//    public void bulletChartFunctionRenaming() throws InterruptedException {
//        performD3TestChrome("/src/d3/chord.html", "/build/functionRenaming/d3/bullet.html");
//    }
    //======================================================================================================

    @Test
    public void circleChartCodeFlatten() throws InterruptedException {
        performD3TestChrome("/src/d3/circle.html", "/build/codeFlattening/d3/circle.html");
    }

    @Test
    public void circleChartFunctionRenaming() throws InterruptedException {
        performD3TestChrome("/src/d3/circle.html", "/build/functionRenaming/d3/circle.html");
    }

    @Test
    public void circleChartRenamingFlatten() throws InterruptedException {
        performD3TestChrome("/src/d3/circle.html", "/build/renameFlattening/d3/circle.html");
    }

    @Test
    public void circleChartFunctionInlining() throws InterruptedException {
        performD3TestChrome("/src/d3/circle.html", "/build/functionInlining/d3/circle.html");
    }

    @Test
    public void circleChartFunctionMerging() throws InterruptedException {
        performD3TestChrome("/src/d3/circle.html", "/build/functionMerging/d3/circle.html");
    }

    @Test
    public void circleChartInliningMerging() throws InterruptedException {
        performD3TestChrome("/src/d3/circle.html", "/build/inliningMerging/d3/circle.html");
    }

    @Test
    public void circleChartStringEncryption() throws InterruptedException {
        performD3TestChrome("/src/d3/circle.html", "/build/stringEncryption/d3/circle.html");
    }

    @Test
    public void circleChartLow() throws InterruptedException {
        performD3TestChrome("/src/d3/circle.html", "/build/low/d3/circle.html");
    }

    @Test
    public void circleChartMedium() throws InterruptedException {
        performD3TestChrome("/src/d3/circle.html", "/build/medium/d3/circle.html");
    }

    @Test
    public void circleChartHigh() throws InterruptedException {
        performD3TestChrome("/src/d3/circle.html", "/build/high/d3/circle.html");
    }

    @Test
    public void circleChartMergeInlineFlattenQpaque() throws InterruptedException {
        performD3TestChrome("/src/d3/circle.html", "/build/mergeInlineFlattenQpaque/d3/circle.html");
    }


//    @Test
//    public void circleChartFlattenOpaque() throws InterruptedException {
//        performD3TestChrome("/src/d3/bubbleChart.html", "/build/flatteningOpaque/d3/circle.html");
//    }
    //======================================================================================================

    @Test
    public void radialTreeChartCodeFlatten() throws InterruptedException {
        performD3TestChrome("/src/d3/radialTree.html", "/build/codeFlattening/d3/radialTree.html");
    }

    @Test
    public void radialTreeChartFunctionRenaming() throws InterruptedException {
        performD3TestChrome("/src/d3/radialTree.html", "/build/functionRenaming/d3/radialTree.html");
    }

    @Test
    public void radialTreeChartRenamingFlatten() throws InterruptedException {
        performD3TestChrome("/src/d3/radialTree.html", "/build/renameFlattening/d3/radialTree.html");
    }

    @Test
    public void radialChartFunctionInlining() throws InterruptedException {
        performD3TestChrome("/src/d3/radial.html", "/build/functionInlining/d3/radial.html");
    }

    @Test
    public void radialChartFunctionMerging() throws InterruptedException {
        performD3TestChrome("/src/d3/radial.html", "/build/functionMerging/d3/radial.html");
    }

    @Test
    public void radialChartInliningMerging() throws InterruptedException {
        performD3TestChrome("/src/d3/radial.html", "/build/inliningMerging/d3/radial.html");
    }

    @Test
    public void radialChartStringEncryption() throws InterruptedException {
        performD3TestChrome("/src/d3/radial.html", "/build/stringEncryption/d3/radial.html");
    }

    @Test
    public void radialChartLow() throws InterruptedException {
        performD3TestChrome("/src/d3/radial.html", "/build/low/d3/radial.html");
    }

    @Test
    public void radialChartMedium() throws InterruptedException {
        performD3TestChrome("/src/d3/radial.html", "/build/medium/d3/radial.html");
    }

    @Test
    public void radialChartHigh() throws InterruptedException {
        performD3TestChrome("/src/d3/radial.html", "/build/high/d3/radial.html");
    }

    @Test
    public void radialChartMergeInlineFlattenQpaque() throws InterruptedException {
        performD3TestChrome("/src/d3/radial.html", "/build/mergeInlineFlattenQpaque/d3/radial.html");
    }

//    @Test
//    public void radialTreeChartFlattenOpaque() throws InterruptedException {
//        performD3TestChrome("/src/d3/bubbleChart.html", "/build/flatteningOpaque/d3/radialTree.html");
//    }
}
