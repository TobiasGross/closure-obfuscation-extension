package de.fau.cs;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * This JUnit testsuite is used to schedule the test classes order the right way. First obfuscation is performed on source
 * javascript. Second Selenium is used to perform comparison of d3 diagrams computed by source and build javascript.
 * @author Tobias Gross
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        CompilerTest.class
        /*,D3Test.class*/
})
public class TestSuite {
}
