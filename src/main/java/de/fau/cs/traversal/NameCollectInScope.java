package de.fau.cs.traversal;

import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * this visitor collects all name nodes. Usually used with a function node as root node. then this collects only names
 * in this function excluding names in child functions
 * @author Tobias Gross
 */
public class NameCollectInScope extends AbstractFunctionScopedCallback {
    private int scopeDepth;
    private List<Node> names;

    public NameCollectInScope() {
        names = new ArrayList<>();
        scopeDepth = 0;
    }

    @Override
    public void enterFunctionScope(NodeTraversal t, Node function) {
        scopeDepth++;
    }

    @Override
    public void exitFunctionScope(NodeTraversal t) {
        scopeDepth--;
    }

    @Override
    public void wrappedVisit(NodeTraversal t, Node n, Node parent) {
        if (scopeDepth < 2 && n.isName()) {
            names.add(n);
        }
    }

    public List<Node> getNames() {
        return names;
    }
}