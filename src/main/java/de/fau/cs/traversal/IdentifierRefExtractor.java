package de.fau.cs.traversal;

import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;
import de.fau.cs.structures.Scope;

/**
 * This ast traversal uses the scope structure to assign names to their declarations.
 * At the end a list of identifiers is created. This consist of function declarations and their references.
 * @author Tobias Gross
 */
public class IdentifierRefExtractor extends AbstractFunctionScopedCallback{
    private Scope scope;

    public IdentifierRefExtractor(Scope root) {
        this.scope = root;
    }

    /**
     * Only name nodes which aren't in function, parameterlist or var context will be added to their corresponding declaration.
     * The other name nodes only occur in declarations.
     * @param t
     * @param n
     * @param parent
     */
    @Override
    public final void wrappedVisit(NodeTraversal t, Node n, Node parent) {
        if(parent == null){
            return;
        }
        if(parent.isFunction())
            return;

        if(parent.isParamList())
            return;

        if(parent.isVar())
            return;

        if(n.isName()){
            scope.addNameRef(n);
        }
    }

    @Override
    public final void enterFunctionScope(NodeTraversal t, Node function) {
        scope = scope.enter();
    }

    /**
     * at scope exit all identifier which represent functions will be added to readyToRename list.
     * @param t
     */
    @Override
    public final void exitFunctionScope(NodeTraversal t) {
        scope = scope.leave();
    }
}
