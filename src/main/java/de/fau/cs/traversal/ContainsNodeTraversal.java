package de.fau.cs.traversal;

import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;
import com.google.javascript.rhino.Token;
import de.fau.cs.passes.FunctionInliningPass;

import java.util.HashSet;
import java.util.Set;

/**
 * this visitor is initialized with a set of tokens. use this class to determine if a given tree contains these types of
 * tokens.
 * @author Tobias Gross
 */
public class ContainsNodeTraversal  extends NodeTraversal.AbstractNodeTypePruningCallback{
    private boolean treeContainsToken;
    public ContainsNodeTraversal(Set<Token> nodeTypes) {
        super(nodeTypes);
        treeContainsToken = false;
    }

    public static ContainsNodeTraversal Function(){
        HashSet<Token> tokenSet = new HashSet<>(1);
        tokenSet.add(Token.FUNCTION);
        return new ContainsNodeTraversal(tokenSet);
    }

    @Override
    public void visit(NodeTraversal t, Node n, Node parent) {
        treeContainsToken = true;
    }

    public boolean containsToken() {
        return treeContainsToken;
    }
}
