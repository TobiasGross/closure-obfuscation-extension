package de.fau.cs.traversal;

import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;

/**
 * this class will traverse ast. each time it passes a function node downward it will call the enter function scope method.
 * on upward it will call exit function scope method. each visited node will trigger wrapped visit method. this extension
 * of AbstractScopedCallback class is necessary because enterScope and exitScope is also called if block scopes are passed.
 * in some situations only function scopes are relevant.
 * @author Tobias Gross
 */
public abstract class AbstractFunctionScopedCallback extends NodeTraversal.AbstractScopedCallback {
    private boolean enteredScope = false;


    @Override
    public void visit(NodeTraversal t, Node n, Node parent) {
        wrappedVisit(t, n, parent);
    }

    @Override
    public void enterScope(NodeTraversal t) {
        if (t.getScopeRoot().isFunction()) {
            enterFunctionScope(t, t.getScopeRoot());
            return;
        }
        if (t.getScopeRoot().isSyntheticBlock()) {
            enterFunctionScope(t, t.getScopeRoot());
            return;
        }
    }

    @Override
    public void exitScope(NodeTraversal t) {
        if (t.getScopeRoot().isFunction() || t.getScopeRoot().isSyntheticBlock()) {
            exitFunctionScope(t);
        }
    }

    abstract public void enterFunctionScope(NodeTraversal t, Node function);

    abstract public void exitFunctionScope(NodeTraversal t);

    abstract public void wrappedVisit(NodeTraversal t, Node n, Node parent);
}
