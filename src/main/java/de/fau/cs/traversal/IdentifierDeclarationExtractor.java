package de.fau.cs.traversal;

import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;
import de.fau.cs.structures.Scope;

/**
 * Ast traversal that extracts all name declarations (variables, parameters, functions). A tree structure is build with the different scopes.
 * All declarations are assigned to their scope.
 * @author Tobias Gross
 */
public class IdentifierDeclarationExtractor extends AbstractFunctionScopedCallback {
    private Scope scope;

    public IdentifierDeclarationExtractor(Scope root) {
        this.scope = root;
    }

    /**
     * Adds declarations to their scope. Declarations are separated in two types: Functions and others (parameter, variables). Anonymous functions are ignored.
     * @param t
     * @param n
     * @param parent
     */
    @Override
    public final void wrappedVisit(NodeTraversal t, Node n, Node parent) {
        if(n.isName()){
            if(parent.isFunction() && !n.getString().equals("")){
                scope.addFuncDecleration(n);
            }
            if(parent.isParamList()){
                scope.addParamDecleration(n);
            }
            if(parent.isVar()){
                scope.addVarDecleration(n);
            }
        }
    }

    /**
     * Method is called when new scope is entered by traversal. This will create a new scope object which is append as child to the old scope object.
     * @param t
     */
    @Override
    public final void enterFunctionScope(NodeTraversal t, Node function) {
        Scope childScope = new Scope(scope, function);
        scope.addChild(childScope);
        scope = childScope;
    }

    /**
     * Method is called when traversal leaves actuell scope. Scope object will be set to parent.
     * @param t
     */
    @Override
    public final void exitFunctionScope(NodeTraversal t) {
        scope = scope.leave();
    }
}
