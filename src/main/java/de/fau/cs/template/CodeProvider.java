package de.fau.cs.template;

import com.google.javascript.jscomp.AbstractCompiler;
import com.google.javascript.jscomp.CompilerInput;
import com.google.javascript.jscomp.SourceFile;
import com.google.javascript.rhino.Node;

import java.io.IOException;
import java.io.InputStream;

/**
 * this class generates trees from template files. for template files see src/main/resources/de/fau/cs/template
 * @author Tobias Gross
 */
public class CodeProvider {
    public static Node getSimpleTrap(AbstractCompiler compiler){
        return getAst(compiler, "trap.js");
    }

    public static Node getSimplePaker(AbstractCompiler compiler){
        return getAst(compiler, "firstPaker.js");
    }

    public static Node getStringDecoder(AbstractCompiler compiler){
        return getAst(compiler, "stringDecoder.js");
    }

    public final static String SEP = "$$$SEP$$$";
    public final static String ENC = "$$$ENC$$$";
    public final static String KEY = "$$$KEY$$$";

    private static Node getAst(AbstractCompiler compiler, String resourceName){
        InputStream stream = CodeProvider.class.getResourceAsStream(resourceName);
        try {
            SourceFile sourceFile = SourceFile.fromInputStream(resourceName, stream);
            CompilerInput compilerInput = new CompilerInput(sourceFile);
            Node root = compilerInput.getAstRoot(compiler);
            return root;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * this method returns a string node which matches the given tamplate string. used to find the placeholders in a tamplate
     * @param parent
     * @param template
     * @return
     */
    public static Node findStringPlaceholder(Node parent, String template){
        for(Node child : parent.children()){
            if(child.isString() && child.getString().equals(template)){
                return child;
            }else{
                Node returnVal = findStringPlaceholder(child, template);
                if(returnVal != null){
                    return returnVal;
                }
            }
        }
        return null;
    }

}
