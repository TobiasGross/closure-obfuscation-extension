package de.fau.cs.passes;

import com.google.javascript.jscomp.AbstractCompiler;
import com.google.javascript.jscomp.CompilerPass;
import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;

import java.util.*;

/**
 * This compiler pass travers the ast and prints all function names to std out
 * @author Tobias Gross
 */
public class FunctionNameExtractionPass implements CompilerPass{
    private final AbstractCompiler compiler;
    private final Set<String> functionNames;
    private final FunctionNameExtractor functionNameExtractor;

    public FunctionNameExtractionPass(AbstractCompiler compiler) {
        this.compiler = compiler;
        functionNames = new HashSet<>();
        functionNameExtractor = new FunctionNameExtractor(functionNames);
    }

    @Override
    public void process(Node externs, Node root) {
        NodeTraversal.traverseEs6(compiler, root, functionNameExtractor);

        List<String> namesAsList = new ArrayList<>(functionNames);
        Collections.sort(namesAsList, String.CASE_INSENSITIVE_ORDER);
        for(String functionName : namesAsList){
            System.out.println(functionName);
        }
    }

    private static final class FunctionNameExtractor extends NodeTraversal.AbstractPostOrderCallback{
        private final Set<String> functionNames;

        public FunctionNameExtractor(Set<String> functionNames) {
            this.functionNames = functionNames;
        }

        @Override
        public void visit(NodeTraversal t, Node n, Node parent) {
            if(n.isFunction()){
                Node name = n.getFirstChild();
                if(!name.getString().equals("")){
                    functionNames.add(name.getString());
                    return;
                }
                if(n.getParent().isName()){
                    name = n.getParent();
                    if(!name.getString().equals("")){
                        functionNames.add(name.getString());
                        return;
                    }
                }
            }
        }
    }
}
