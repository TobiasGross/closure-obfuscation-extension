package de.fau.cs.passes;

import com.google.javascript.jscomp.AbstractCompiler;
import com.google.javascript.jscomp.CompilerPass;
import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;
import de.fau.cs.gen.opaqueValues.ArrayAliasingGenerator;
import de.fau.cs.util.BlockHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tobias Gross
 */
public class OpaqueValuePass implements CompilerPass {
    private final List<Node> numbers;
    private final List<Node> trues;
    private final List<Node> falses;
    private Node script;
    private final AbstractCompiler abstractCompiler;
    private final ArrayAliasingGenerator arrayAliasingGenerator;
    private BlockHolder blockHolder;

    private NumberAndBooleanExtractor numberAndBooleanExtractor;

    public OpaqueValuePass(AbstractCompiler abstractCompiler, long seed) {
        this.trues = new ArrayList<>();
        this.falses = new ArrayList<>();
        this.numbers = new ArrayList<>();
        this.abstractCompiler = abstractCompiler;
        numberAndBooleanExtractor = new NumberAndBooleanExtractor();
        arrayAliasingGenerator = new ArrayAliasingGenerator(10, 10, 0.2, seed);
        blockHolder = BlockHolder.getInstance();
    }

    /**
     * collects all numbers and boolean values in script file and exchange them with values from alias array
     * also add statements which transform the alias array, so that invariants are kept and statements which are never
     * executed which alters the array with no regard to the invariants
     * @param externs
     * @param root
     */
    @Override
    public void process(Node externs, Node root) {
        NodeTraversal.traverseEs6(abstractCompiler, root, numberAndBooleanExtractor);

        script.addChildToFront(arrayAliasingGenerator.getGlobalArrayInit());

        for(Node number : numbers){
            long value = (long)number.getDouble();
            Node newValue = arrayAliasingGenerator.getNumber(value);
            changeNode(number, newValue);
        }
        for(Node truee : trues){
            Node newTrue = arrayAliasingGenerator.getTrue();
            changeNode(truee, newTrue);
        }
        for(Node falsee : falses){
            Node newFalse = arrayAliasingGenerator.getFalse();
            changeNode(falsee, newFalse);
        }

        for(Node block: blockHolder.getBlockNodesForFlux()){
            Node flux = arrayAliasingGenerator.getFlux();
            block.addChildToFront(flux);
        }

        for(Node block: blockHolder.getBlockNodesForFakeFlux()){
            Node flux = arrayAliasingGenerator.getFakeFlux();
            block.addChildToFront(flux);
        }

    }

    /**
     * exchanges origNode with newNode
     * @param origNode
     * @param newNode
     */
    private void changeNode(Node origNode, Node newNode){
        Node parent = origNode.getParent();
        parent.addChildBefore(newNode, origNode);
        parent.removeChild(origNode);
    }

    /**
     * this visitor fills the fields trues, falses, numbers and script
     */
    private class NumberAndBooleanExtractor extends NodeTraversal.AbstractPostOrderCallback {
        @Override
        public void visit(NodeTraversal t, Node n, Node parent) {
            if (n.isNumber()) {
                double value = n.getDouble();
                if ((value == Math.floor(value)) && !Double.isInfinite(value) && value != 0) {
                    numbers.add(n);
                }
            }

            if (n.isTrue()) {
                trues.add(n);
            }

            if (n.isFalse()) {
                falses.add(n);
            }

            if(n.isScript() && script == null){
                script = n;
            }
        }
    }
}
