package de.fau.cs.passes;

import com.google.javascript.jscomp.AbstractCompiler;
import com.google.javascript.jscomp.CompilerPass;
import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;
import com.google.javascript.rhino.Token;
import de.fau.cs.template.CodeProvider;

import java.util.*;

/**
 * this pass will encrypt all strings in input script. this is done with these steps:
 * 1. collect all string nodes
 * 2. join the strings with a delimiter to a great string
 * 3. xor encrypt the resulting string with a random string of the same length
 * 4. place resulting encrypted string and key in input script
 * 5. add statements to decrypt and split the string. that results in a array of strings
 * 6. replace every string in input script with a reference to this array
 * @author Tobias Gross
 */
public class StringEncryptionPass implements CompilerPass {
    private AbstractCompiler compiler;
    private Random random;
    private List<String> strings;
    private String separator;

    private static final String SEPARATOR_START_END_TOKEN = ":";
    private static final String SEPARATOR_FILL_TOKEN = "|";

    private static final String STRINGS_NAME = "___strings";
    private String keyString;
    private String decString;


    public StringEncryptionPass(AbstractCompiler compiler, long seed) {
        this.compiler = compiler;
        this.random = new Random(seed);
        this.strings = new ArrayList<>();
    }

    @Override
    public void process(Node externs, Node root) {
        List<Node> stringNodes = new ArrayList<>();
        NodeTraversal.traverseEs6(compiler, root, new GetpropToGetelemTraversal(stringNodes));
        if (!stringNodes.isEmpty()) {
            buildStringArray(stringNodes);
            separator = findSeparator();
            String encString = createEncryptedString();

            //find parent to append code
            Node parent = root;
            while (!parent.isScript()) {
                parent = parent.getFirstChild();
            }
            attachStringArrayCalculation(parent, separator);
            attachEncAndKey(parent, keyString, encString);
            changeStringNodesToArrayAccess(stringNodes, strings);
        }
    }

    /**
     * replaces all strings in the input script with a reference to the string array
     * @param stringNodes
     * @param strings
     */
    private void changeStringNodesToArrayAccess(List<Node> stringNodes, List<String> strings) {
        for (Node stringNode : stringNodes) {
            int index = strings.indexOf(stringNode.getString());
            Node getElem = new Node(Token.GETELEM);

            stringNode.getParent().addChildBefore(getElem, stringNode);
            stringNode.detachFromParent();

            getElem.addChildToBack(Node.newString(Token.NAME, STRINGS_NAME));
            getElem.addChildToBack(Node.newNumber(index));
        }
    }

    /**
     * replaces the key and encrypted string placeholder in the code fragment with the right ones
     * @param parent
     * @param key
     * @param enc
     */
    private void attachEncAndKey(Node parent, String key, String enc) {
        Node keyStringPlaceholder = CodeProvider.findStringPlaceholder(parent, CodeProvider.KEY);
        Node encStringPlaceholder = CodeProvider.findStringPlaceholder(parent, CodeProvider.ENC);

        keyStringPlaceholder.getParent().addChildBefore(Node.newString(key), keyStringPlaceholder);
        keyStringPlaceholder.detachFromParent();

        encStringPlaceholder.getParent().addChildBefore(Node.newString(enc), encStringPlaceholder);
        encStringPlaceholder.detachFromParent();
    }

    /**
     * this method takes a tree from code provider and attach them in front of the input script. replaces the the separator
     * placeholder node with the created separator
     * @param parent
     * @param separator
     */
    private void attachStringArrayCalculation(Node parent, String separator) {
        Node decoder = CodeProvider.getStringDecoder(compiler);
        Node seperatorNode = CodeProvider.findStringPlaceholder(decoder, CodeProvider.SEP);
        while(decoder.hasChildren()){
            parent.addChildToFront(decoder.getLastChild().detachFromParent());
        }

        seperatorNode.getParent().addChildBefore(Node.newString(separator), seperatorNode);
        seperatorNode.detachFromParent();
    }

    /**
     * builds a set of string from given strings. adds the strings to field strings
     * @param stringNodes
     */
    private void buildStringArray(List<Node> stringNodes) {
        Set<String> stringSet = new HashSet<>(stringNodes.size());
        for (Node str : stringNodes) {
            stringSet.add(str.getString());
        }
        strings.addAll(stringSet);
    }

    /**
     * first concatenates the strings of the array with the delimiter symbol in between
     * second, calls the buil key method and the xor method
     * @return encrypted string
     */
    private String createEncryptedString() {
        StringBuilder builder = new StringBuilder();
        for (String s : strings) {
            builder.append(s);
            builder.append(separator);
        }
        decString = builder.toString();

        keyString = buildKey(decString.length());
        String enc = xor(decString, keyString);

        return enc;
    }

    /**
     * determines random chars and appends them to a string of "length" size
     * @param length
     * @return
     */
    private String buildKey(int length) {
        StringBuilder keyBuilder = new StringBuilder();
        while (length > 0) {
            char tmp = (char) random.nextInt();
            keyBuilder.append(tmp);
            length--;
        }
        return keyBuilder.toString();
    }

    /**
     * takes one character at a time of each input string. performes xor operation and concatenates the resulting chars to
     * a encrypted string
     * @param first
     * @param second
     * @return
     */
    private String xor(String first, String second) {
        char[] firstChars = first.toCharArray();
        char[] secondChars = second.toCharArray();
        for (int i = 0; i < firstChars.length; i++) {
            firstChars[i] = (char) (firstChars[i] ^ secondChars[i]);
        }
        return new String(firstChars);
    }

    /**
     * build up a string which can be used as delimiter. this string must not contained in another string in the array.<br>
     * S -&gt; :A:<br>
     * A -&gt; A|<br>
     * A -&gt; e<br>
     * @return assambled suitable seperator
     */
    private String findSeparator() {
        int fillTimes = 1;
        boolean collide = false;
        String separator;

        do {
            separator = SEPARATOR_START_END_TOKEN;
            for (int i = 0; i < fillTimes; i++) {
                separator += SEPARATOR_FILL_TOKEN;
            }
            separator += SEPARATOR_START_END_TOKEN;


            collide = false;
            for (String str : strings) {
                if (str.contains(separator)) {
                    collide = true;
                    break;
                }
            }
            fillTimes++;

        } while (collide);
        return separator;
    }

    /**
     * this ast visitor will replace all getprop nodes to getelem:
     * example.first =&gt; example['first']
     * also collects all string nodes ( + the new created strings)
     */
    private static final class GetpropToGetelemTraversal extends NodeTraversal.AbstractPostOrderCallback {
        private List<Node> stringNodes;

        public GetpropToGetelemTraversal(List<Node> stringNodes) {
            this.stringNodes = stringNodes;
        }

        @Override
        public void visit(NodeTraversal t, Node n, Node parent) {
            if (n.isGetProp()) {
                Node getElem = new Node(Token.GETELEM);
                for (Node child : n.children()) {
                    child.detachFromParent();
                    getElem.addChildToBack(child);
                }
                n.getParent().addChildBefore(getElem, n);
                n.detachFromParent();
            }
            if (n.isString() && !n.getParent().isRegExp()) {
                stringNodes.add(n);
            }
        }
    }
}
