package de.fau.cs.passes;

import com.google.javascript.jscomp.CompilerPass;
import com.google.javascript.rhino.Node;
import com.google.javascript.rhino.Token;

/**
 * this pass wraps each input script in its own namespace by an enclosing anonymous function call:
 * (function(){
 *     //script content
 * })();
 * @author Tobias Gross
 */
public class WrapInFunctionPass implements CompilerPass {
    @Override
    public void process(Node externs, Node root) {
        for(Node child : root.children()){
            Node aBlock = new Node(Token.BLOCK);
            Node expRes = createFunctionWrapper(aBlock);
            for(Node scriptChild : child.children()){
                aBlock.addChildToBack(scriptChild.detachFromParent());
            }
            child.addChildToFront(expRes);
        }

    }

    private Node createFunctionWrapper(Node block){
        Node exprRes = new Node(Token.EXPR_RESULT);
        Node call = new Node(Token.CALL);
        exprRes.addChildToBack(call);
        Node func = new Node(Token.FUNCTION);
        call.addChildToBack(func);
        func.addChildToBack(Node.newString(Token.NAME, ""));
        func.addChildToBack(new Node(Token.PARAM_LIST));
        func.addChildToBack(block);

        return exprRes;
    }
}
