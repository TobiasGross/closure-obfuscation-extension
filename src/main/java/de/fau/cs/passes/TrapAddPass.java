package de.fau.cs.passes;

import com.google.javascript.jscomp.AbstractCompiler;
import com.google.javascript.jscomp.CompilerPass;
import com.google.javascript.rhino.Node;
import de.fau.cs.template.CodeProvider;

/**
 * this pass adds code to the input script which will cause a debugger to trap. see CodeProvider class for details.
 * @author Tobias Gross
 */
public class TrapAddPass implements CompilerPass {
    private AbstractCompiler abstractCompiler;

    public TrapAddPass(AbstractCompiler abstractCompiler) {
        this.abstractCompiler = abstractCompiler;
    }

    @Override
    public void process(Node externs, Node root) {
        Node trapCode = CodeProvider.getSimpleTrap(abstractCompiler);
        while(trapCode.hasChildren()){
            root.getFirstChild().addChildToFront(trapCode.getLastChild().detachFromParent());
        }
    }
}
