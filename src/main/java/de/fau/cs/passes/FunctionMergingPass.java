package de.fau.cs.passes;

import com.google.javascript.jscomp.AbstractCompiler;
import com.google.javascript.jscomp.CompilerPass;
import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;
import com.google.javascript.rhino.Token;
import de.fau.cs.structures.Identifier;
import de.fau.cs.structures.Scope;
import de.fau.cs.traversal.IdentifierDeclarationExtractor;
import de.fau.cs.traversal.IdentifierRefExtractor;
import de.fau.cs.traversal.NameCollectInScope;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * this pass collects leaf functions and merge them together with other leaf functions with the same function as parent.
 * the merging process is divided into two parts. first the merged function is created which has n + 1 parameter. n is the max
 * parameter count of merged functions. with the n + 1 parameter the decision is taken which function should be executed.
 * the execution of the functions is performed with a call.
 * Second, the calls are inlined with the function inlining pass.
 * @author Tobias Gross
 */
public class FunctionMergingPass implements CompilerPass{
    private final static String ARGUMENTS = "arguments";
    private final static String WHICH_PARAM_NAME = "__which";
    private final static String PARAM_PREFIX = "__param";
    public final static String FUNC_PREFIX = "__mergedFunc";


    private final AbstractCompiler compiler;
    private final Random random;
    private Scope rootScope;

    public FunctionMergingPass(AbstractCompiler compiler, long seed) {
        this.compiler = compiler;
        random = new Random(seed);
    }

    @Override
    public void process(Node externs, Node root) {
        builUpScopeTree(root);
        List<MergeBundle> bundles = createMergeBundles(rootScope);
        bundles = filterMergeBundles(bundles);
        transformToMergedFunctions(bundles);
    }

    /**
     * this method creates the scope chains, which form a tree
     * @param root
     */
    private void builUpScopeTree(Node root) {
        rootScope = new Scope(null, null);
        NodeTraversal.traverseEs6(compiler, root, new IdentifierDeclarationExtractor(rootScope));
        NodeTraversal.traverseEs6(compiler, root, new IdentifierRefExtractor(rootScope));
    }

    /**
     * bundles function which can be merged (are leaf functions and have the same parent function)
     * this method calls itself recursively down the scope tree
     * @param scope
     * @return
     */
    private List<MergeBundle> createMergeBundles(Scope scope){
        List<MergeBundle> bundles = new ArrayList<>();
        MergeBundle aBundle = new MergeBundle();
        for(Identifier function : scope.getFunctionIdentifiers()){
            Scope functionScope = rootScope.getFunctionScope(function.getDecl().getParent());
            if(functionScope.getChildren().isEmpty()){
                aBundle.addFunction(function);
            }
        }
        Node functionNode = scope.getFunction();
        if(functionNode != null){
            aBundle.setBlockToAddMergedFunction(functionNode.getLastChild());
        }
        bundles.add(aBundle);

        for(Scope childScope : scope.getChildren()){
            bundles.addAll(createMergeBundles(childScope));
        }
        return bundles;
    }

    /**
     * removes functions from bundles which use the arguments construct, because they often have variable argument count
     * which can't be arranged with this merging strategy
     * filters all merge bundles which have less than 2 functions to merge
     * @param bundles
     * @return
     */
    private List<MergeBundle> filterMergeBundles(List<MergeBundle> bundles){
        List<MergeBundle> bundlesToRemove = new ArrayList<>();
        for(MergeBundle bundle : bundles){
            if(!bundle.removeFunctionDefsWithArgumentsUsage()){
                bundlesToRemove.add(bundle);
            }
        }

        bundles.removeAll(bundlesToRemove);
        return bundles;
    }

    /**
     * iterates over bundles and proceed the merging process to each bundle
     * @param bundles
     */
    private void transformToMergedFunctions(List<MergeBundle> bundles){
        for(MergeBundle bundle: bundles){
            transformToMergedFunction(bundle);
        }
    }

    /**
     * creates a merged function from a given bundle. adds the created function in front of defined block in bundle
     * also alters the calls to merged functions to point to the created function
     * @param bundle
     */
    private void transformToMergedFunction(MergeBundle bundle){
        int max = getMaxParameter(bundle);
        Node mergedFunc = createMergedFunctionFrame(max);

        bundle.getBlockToAddMergedFunction().addChildToFront(mergedFunc);

        int which = 0;
        for(Node def : bundle.getDefs()){
            Node ifNode = createConditional(which);

            Node returnNode = new Node(Token.RETURN);
            ifNode.getLastChild().addChildToBack(returnNode);

            Node callNode = createCallTemplate(max);
            returnNode.addChildToBack(callNode);
            //inserts the name of function to call
            callNode.addChildrenToFront(def.cloneTree());

            mergedFunc.getLastChild().addChildToBack(ifNode);
            which++;
        }

        which--;
        for(int i = which; i >= 0; i--){
            List<Node> callNameNodes = bundle.getCalls(i);
            Node whichNode = Node.newNumber(i);
            for(Node nameNode : callNameNodes){
                try {
                    Node call = nameNode.getParent();
                    fillUpCallParams(call, max);
                    call.addChildToBack(whichNode.cloneTree());
                    call.removeFirstChild();
                    call.addChildrenToFront(mergedFunc.getFirstChild().cloneTree());
                }catch(NullPointerException e){
                    System.err.println("Node: " + nameNode);
                    throw e;
                }
            }
        }
    }

    /**
     * this method adds bogus parameter to calls which uses less parameter than the created function provides
     * @param callNode
     * @param paramCount
     */
    private void fillUpCallParams(Node callNode, int paramCount){
        int countBogusInsert = paramCount - callNode.getChildCount() + 1;
        List<Node> bogusParams = getNamesInScope(callNode, countBogusInsert);
        for(Node param : bogusParams){
            callNode.addChildToBack(param);
        }
    }

    /**
     * checks all parents of provided node. if all parent nodes are types of synthetic block or script true is returned,
     * otherwise false
     * @param aNode
     * @return
     */
    private boolean onlySyntheticBlocksAndScriptAsParent(Node aNode){
        aNode = aNode.getParent();
        while(aNode != null){
            if(!(aNode.isSyntheticBlock() || aNode.isScript())){
                return false;
            }
            aNode = aNode.getParent();
        }
        return true;
    }

    /**
     * determines the scope of provided node and choose random name nodes. count name nodes are chosen. if there aren't enough
     * names, number nodes are generated
     * @param aNode
     * @param count
     * @return
     */
    private List<Node> getNamesInScope(Node aNode, int count){
        List<Node> nameNodes = new ArrayList<>();
        Node includingFuncNode = aNode;
        while (!includingFuncNode.isFunction() && !(includingFuncNode.isSyntheticBlock() && onlySyntheticBlocksAndScriptAsParent(includingFuncNode) && !includingFuncNode.isScript())) {
            includingFuncNode = includingFuncNode.getParent();
        }

        Scope scope = rootScope.getFunctionScope(includingFuncNode);
        List<Identifier> identifiers = scope.getIdentifiers();
        Collections.shuffle(identifiers, random);

        while(!identifiers.isEmpty() && count > 0){
            nameNodes.add(identifiers.remove(0).getDecl().cloneNode());
            count --;
        }

        while(count > 0){
            nameNodes.add(Node.newNumber(random.nextInt()));
            count--;
        }
        return nameNodes;
    }

    /**
     * creates an call tree with given parameter count
     * @param paramCount
     * @return
     */
    private Node createCallTemplate(int paramCount){
        Node call = new Node(Token.CALL);
        for(int i = 0; i < paramCount; i++){
            Node name = Node.newString(Token.NAME, PARAM_PREFIX + i);
            call.addChildToBack(name);
        }
        return call;
    }

    /**
     * creates a if construct which checks the last parameter of merged function (which parameter) of equality to given integer
     * @param condition
     * @return
     */
    private Node createConditional(int condition){
        Node ifNode = new Node(Token.IF);
        Node eqNode = new Node(Token.EQ);
        Node left = Node.newString(Token.NAME, WHICH_PARAM_NAME);
        Node right = Node.newNumber(condition);
        eqNode.addChildToBack(left);
        eqNode.addChildToBack(right);
        ifNode.addChildToBack(eqNode);
        ifNode.addChildToBack(new Node(Token.BLOCK));
        return ifNode;
    }

    /**
     * creates empty function deceleration with paramCount + 1 parameter and a random function name
     * @param paramCount
     * @return function node
     */
    private Node createMergedFunctionFrame(int paramCount){
        Node paramList = new Node(Token.PARAM_LIST);
        for(int i = 0; i < paramCount; i++){
            Node name = Node.newString(Token.NAME, PARAM_PREFIX + i);
            paramList.addChildToBack(name);
        }
        Node whichName = Node.newString(Token.NAME, WHICH_PARAM_NAME);
        paramList.addChildToBack(whichName);
        Node func = new Node(Token.FUNCTION);
        Node name = Node.newString(Token.NAME, generateFunctionName());
        Node block = new Node(Token.BLOCK);

        func.addChildToBack(name);
        func.addChildToBack(paramList);
        func.addChildToBack(block);
        return func;
    }

    /**
     * generates random function name with static prefix
     * @return
     */
    private String generateFunctionName(){
        return FUNC_PREFIX + random.nextInt(Integer.MAX_VALUE);
    }

    /**
     * iterates over functions in given bundle, determine parameter count and returns the maximum
     * @param bundle
     * @return
     */
    private int getMaxParameter(MergeBundle bundle){
        int max = 0;
        for(Node func : bundle.getDefs()){
            int paramCount = func.getParent().getChildAtIndex(1).getChildCount();
            if(paramCount > max){
                max = paramCount;
            }
        }
        return max;
    }

    /**
     * bundles function identifiers of functions which can be merged together
     */
    private class MergeBundle{
        private Node blockToAddMergedFunction;
        private List<Identifier> functionDefs = new ArrayList<>();

        /**
         * this method removes functions with from bundle which use the arguments identifier
         * @return true if function count in bundle is greater then one after removal
         */
        public boolean removeFunctionDefsWithArgumentsUsage(){
            List<Identifier> functionsToRemove = new ArrayList<>();

            for(Identifier func : functionDefs){
                NameCollectInScope nameCollectInScope = new NameCollectInScope();
                NodeTraversal.traverseEs6(compiler, func.getDecl().getParent().getLastChild(), nameCollectInScope);
                boolean hasArguementsUsage = nameCollectInScope.getNames().stream().anyMatch(node -> node.getString().equals(ARGUMENTS));
                if(hasArguementsUsage){
                    functionsToRemove.add(func);
                }

            }
            functionDefs.removeAll(functionsToRemove);
            return functionDefs.size() > 1;
        }

        /**
         * adds a function identifier to bundle
         * @param identifier
         * @return
         */
        public boolean addFunction(Identifier identifier) {
            return functionDefs.add(identifier);
        }

        /**
         * @return block which to add the generated merged function
         */
        public Node getBlockToAddMergedFunction() {
            return blockToAddMergedFunction;
        }

        /**
         *
         * @param blockToAddMergedFunction block which to add the generated merged function
         */
        public void setBlockToAddMergedFunction(Node blockToAddMergedFunction) {
            this.blockToAddMergedFunction = blockToAddMergedFunction;
        }

        /**
         * @return nodes of all functions in this bundle
         */
        public List<Node> getDefs(){
            List<Node> nodes = new ArrayList<>();
            for(Identifier i : functionDefs){
                nodes.add(i.getDecl());
            }
            return nodes;
        }

        /**
         * for a given index of function in this bundle, returns all referencing name nodes
         * @param indexOfDef
         * @return
         */
        public List<Node> getCalls(int indexOfDef){
            return functionDefs.get(indexOfDef).getRefs().stream().filter(node -> node.getParent() != null && node.getParent().isCall() && node.getParent().getFirstChild().equals(node)).collect(Collectors.toList());
        }
    }
}

