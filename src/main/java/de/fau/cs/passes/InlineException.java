package de.fau.cs.passes;

/**
 * This exception will be thrown if in the inlining process a precondition is not valid and the inlining for this function
 * has to be aborted
 * @author Tobias Gross
 */
public class InlineException extends Exception{
}
