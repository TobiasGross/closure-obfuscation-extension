package de.fau.cs.passes;

import com.google.javascript.jscomp.AbstractCompiler;
import com.google.javascript.jscomp.CompilerPass;
import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;
import de.fau.cs.structures.Scope;
import de.fau.cs.traversal.IdentifierDeclarationExtractor;
import de.fau.cs.traversal.IdentifierRefExtractor;
import de.fau.cs.util.AngularNameProvider;
import de.fau.cs.util.NameProvider;

/**
 * This compiler pass renames all types of declarations (functions, variables, parameter).
 * All identifier pointing to a renamed function are altered accordingly. Using a seed, the result can be reproduced.
 * This pass traverse the ast two times. First, all declarations are extracted.
 * Second, names are assigned to right declarations. All matching declarations and identifier are renamed the same.
 * Functions are renamed with names provided by Angular name provider or a custom one.
 * @author Tobias Gross
 */
public class IdentifierRenamePass implements CompilerPass {
    private final AbstractCompiler compiler;
    private final Scope rootScope;
    private final NameProvider nameProvider;

    private final IdentifierDeclarationExtractor identifierDeclarationExtractor;
    private final IdentifierRefExtractor identifierRefExtractor;

    /**
     *
     * @param compiler used for AST traversal methods
     * @param seed used to choose same function names every time the same seed is used
     */
    public IdentifierRenamePass(AbstractCompiler compiler, long seed) {
        this.rootScope = new Scope(null, null);
        this.identifierDeclarationExtractor = new IdentifierDeclarationExtractor(rootScope);
        this.compiler = compiler;
        this.nameProvider = new AngularNameProvider(seed);
        this.identifierRefExtractor = new IdentifierRefExtractor(rootScope);

    }

    /**
     * in contrast to the other constructor, this will take a custom name provider instead of creating a default name provider
     * @param compiler
     * @param nameProvider
     */
    public IdentifierRenamePass(AbstractCompiler compiler, NameProvider nameProvider) {
        this.rootScope = new Scope(null, null);
        this.identifierDeclarationExtractor = new IdentifierDeclarationExtractor(rootScope);
        this.compiler = compiler;
        this.nameProvider = nameProvider;
        this.identifierRefExtractor = new IdentifierRefExtractor(rootScope);
    }

    /**
     * This method is called by the compiler at the assigned time (see CLIRunner.createOptions)
     * @param externs ast that represents extern files
     * @param root ast that represents source files
     */
    @Override
    public final void process(Node externs, Node root) {
        NodeTraversal.traverseEs6(compiler, root, identifierDeclarationExtractor);
        NodeTraversal.traverseEs6(compiler, root, identifierRefExtractor);
        rootScope.renameFunctions(nameProvider);
        rootScope.renameVariables(nameProvider);
        rootScope.renameParameter(nameProvider);
    }
}
