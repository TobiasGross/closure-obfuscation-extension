package de.fau.cs.passes;

import com.google.javascript.jscomp.*;
import com.google.javascript.rhino.Node;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Tobias Gross on 06.07.16.
 */
public class TestPass implements CompilerPass{
    private final FunctionListExtractor functionListExtractor;
    private final CallExtractor callExtractor;
    private AbstractCompiler compiler;

    private final Map<Node, FunctionRecord> functionMap = new LinkedHashMap<>();

    public TestPass(AbstractCompiler compiler) {
        this.compiler = compiler;
        this.functionListExtractor = new FunctionListExtractor(functionMap);
        this.callExtractor = new CallExtractor();
    }

    @Override
    public void process(Node externs, Node root) {
        //NodeTraversal.traverseEs6(compiler,root,functionListExtractor);
        //NodeTraversal.traverseEs6(compiler, root, callExtractor);
        //System.out.println(functionMap);
        NodeTraversal.traverseEs6(compiler, root, new IfPrint());
    }

    private static class FunctionRecord implements Serializable {
        private static final long serialVersionUID = 1L;

        public final int id;
        public final Node parent;
        public String name;

        FunctionRecord(int id, Node parent, String name) {
            this.id = id;
            this.parent = parent;
            this.name = name;
        }

        @Override
        public String toString() {
            return "FunctionRecord{" +
                    "id=" + id +
                    ", parent=" + parent +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    private static class FunctionListExtractor extends NodeTraversal.AbstractPostOrderCallback {
        private final Map<Node, FunctionRecord> functionMap;
        private int nextId = 0;

        FunctionListExtractor(Map<Node, FunctionRecord> functionMap) {
            this.functionMap = functionMap;
        }

        @Override
        public void visit(NodeTraversal t, Node n, Node parent) {
            if (n.isFunction()) {
                Node functionNameNode = n.getFirstChild();
                String functionName = functionNameNode.getString();

                Node enclosingFunction = t.getEnclosingFunction();

                functionMap.put(n,
                        new FunctionRecord(nextId, enclosingFunction, functionName));
                nextId++;
            }
        }
    }

    private static class CallExtractor extends NodeTraversal.AbstractPostOrderCallback {

        @Override
        public void visit(NodeTraversal t, Node n, Node parent) {
            if(n.isCall()){
                if(n.isFromExterns()){
                    System.out.println("Extern: " + n.getFirstChild());
                }else{
                    System.out.println("Intern: " + n.getFirstChild());
                }
            }
        }
    }

    private static class IfPrint extends NodeTraversal.AbstractPostOrderCallback{

        @Override
        public void visit(NodeTraversal t, Node n, Node parent) {
            System.out.println("No side effect: " + n.isNoSideEffectsCall());
            Scope s = t.getScope();
            System.out.println("=======" + s);
            if (s == null) {
                return;
            }
            for (Var sym : s.getAllSymbols()) {
                System.out.println(sym);
            }
            System.out.println("=================");
        }

    }
}
