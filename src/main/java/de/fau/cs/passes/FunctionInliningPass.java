package de.fau.cs.passes;

import com.google.javascript.jscomp.AbstractCompiler;
import com.google.javascript.jscomp.CompilerPass;
import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;
import com.google.javascript.rhino.Token;
import de.fau.cs.structures.Identifier;
import de.fau.cs.structures.Scope;
import de.fau.cs.traversal.*;
import de.fau.cs.util.NameProvider;
import de.fau.cs.util.RandomNameProvider;

import java.util.*;
import java.util.stream.Collectors;

/**
 * this pass finds suitable functions for inlining (all leaf functions). all calls of this functions are candidates for inlining.
 * all used identifiers in an inline function (the source scope) must reference the same at the inline spot (the target scope).
 * if this isn't given, inlining this call isn't possible. Another no-go for inlining are recursive functions.
 * @author Tobias Gross
 */
public class FunctionInliningPass implements CompilerPass {
    private AbstractCompiler compiler;
    private int times;
    private Scope rootScope;
    private final static String UNDEFINED = "undefined";
    private final static String ARGUMENTS = "arguments";

    private final static String BLOCK_LABEL_PREFIX = "__X";

    private Random random;
    private long seed;
    private Set<String> usedBlockLabels;
    private NameProvider randomNameProvider;
    private List<DefCallPair> spotsForInlining;
    private boolean processOnlyMergingFunctions;

    public FunctionInliningPass(AbstractCompiler compiler, int times, long seed) {
        this.compiler = compiler;
        this.times = times;
        random = new Random(seed);
        usedBlockLabels = new HashSet<>();
        this.seed = seed;
        spotsForInlining = new ArrayList<>();
        this.processOnlyMergingFunctions = false;
    }

    /**
     *
     * @param compiler
     * @param times
     * @param seed
     * @param processOnlyMergingFunctions set to true if this pass is used after function merging pass
     */
    public FunctionInliningPass(AbstractCompiler compiler, int times, long seed, boolean processOnlyMergingFunctions) {
        this.compiler = compiler;
        this.times = times;
        random = new Random(seed);
        usedBlockLabels = new HashSet<>();
        this.seed = seed;
        spotsForInlining = new ArrayList<>();
        this.processOnlyMergingFunctions = processOnlyMergingFunctions;
    }

    @Override
    public void process(Node externs, Node root) {
        while (times > 0) {
            randomNameProvider = new RandomNameProvider(seed, Collections.EMPTY_SET, (processOnlyMergingFunctions ? "m" : "") + times);
            spotsForInlining.clear();
            builUpScopeTree(root);
            List<Identifier> functionIdentifiers = rootScope.getAllFunctionIdentifiers();
            for (Identifier funcIdent : functionIdentifiers) {
                findPossipleSpots(funcIdent);
            }
            for (DefCallPair pair : spotsForInlining) {
                try {
                    inlineCall(pair.def, pair.call, pair.usesArgument);
                }catch(InlineException e){
                }
            }
            times--;
        }
    }

    /**
     * this method creates the scope chains, which form a tree
     * @param root
     */
    private void builUpScopeTree(Node root) {
        rootScope = new Scope(null, null);
        NodeTraversal.traverseEs6(compiler, root, new IdentifierDeclarationExtractor(rootScope));
        NodeTraversal.traverseEs6(compiler, root, new IdentifierRefExtractor(rootScope));
    }

    /**
     * arguments keyword has no special node in ast. it is represented as name node (same as other identifier).
     * this method checks occurrence of this keyword and removes it from identifier list.
     * @param nodes
     * @return
     */
    private boolean removeArgumentsName(List<Node> nodes) {
        List<Node> toRemove = new ArrayList<>();
        for (Node node : nodes) {
            if (node.getString().equals(ARGUMENTS)) {
                toRemove.add(node);
            }
        }
        nodes.removeAll(toRemove);
        return !toRemove.isEmpty();
    }

    /**
     * marks spots for inlining with creating a DefCallPair object. this object holds the function node and the call node.
     * a flag indicates functions which uses the arguments keyword.
     * filters recursive functions.
     * filters other call spots which can't be inlined (see filterCallsWhichCanBeInlined method).
     * @param funcIdent
     */
    private void findPossipleSpots(Identifier funcIdent) {
        Node functionToInline = funcIdent.getDecl().getParent();
        if (!isTopLevelFunction(functionToInline)) {
            return;
        }
        Scope functionToInlineScope = getScopeForFunction(functionToInline);

        List<Node> namesNotInScope = getAllNameNodesNotRefToScope(functionToInline, functionToInlineScope);

        //don't inline recursive functions
        for(Node nameNotInScope : namesNotInScope){
            if(funcIdent.getRefs().contains(nameNotInScope)){
                return;
            }
        }
        boolean usesArguments = removeArgumentsName(namesNotInScope);

        List<Node> inlineCalls = funcIdent.getRefs();
        inlineCalls = inlineCalls.stream().filter(node -> node.getParent().isCall() && node.getParent().getIndexOfChild(node) == 0).collect(Collectors.toList());

        filterCallsWhichCanBeInlined(namesNotInScope, functionToInlineScope, inlineCalls);
        for (Node inlineCall : inlineCalls) {
            DefCallPair pair = new DefCallPair(functionToInline, inlineCall.getParent(), usesArguments);
            spotsForInlining.add(pair);
        }
    }

    /**
     * main method for inline procedure. takes a call node a function node and a hint, if arguments keyword is used.
     * all statements of the inlined function are be placed before the call. parameter are transformed to variables, which are
     * initialized with the call values. if the arguments keyword is used, it's replaced with an array containing the same values.
     * function return mechanism is replaced with an assignment to a generated variable.
     * @param functionToInline
     * @param callForInline
     * @param withArugment
     * @throws InlineException if inlining can't be performed
     */
    private void inlineCall(Node functionToInline, Node callForInline, boolean withArugment) throws InlineException {
        Node exprRes = callForInline;
        functionToInline = functionToInline.cloneTree();
        while (exprRes != null && !exprRes.isExprResult() && !exprRes.isReturn() && !exprRes.isVar() && !exprRes.isIf()) {
            exprRes = exprRes.getParent();
        }
        if (exprRes == null) {
            throw new InlineException();
        }

        Node argumentsVarName = null;
        Node arrayLit = null;
        if (withArugment) {
            argumentsVarName = createReturnVar(callForInline);
            arrayLit = new Node(Token.ARRAYLIT);
            Node assign = new Node(Token.ASSIGN);
            assign.addChildToBack(argumentsVarName.cloneTree());
            assign.addChildToBack(arrayLit);
            Node arrayExpRes = new Node(Token.EXPR_RESULT);
            arrayExpRes.addChildToBack(assign);
            exprRes.getParent().addChildBefore(arrayExpRes, exprRes);
        }


        Node returnVarName = null;
        if (!callForInline.getParent().isExprResult()) {
            returnVarName = createReturnVar(callForInline);
        }

        Node funcNode = callForInline;
        while (!funcNode.isFunction() && !(funcNode.isSyntheticBlock() && funcNode.getParent() == null)) {
            funcNode = funcNode.getParent();
        }
        renameNames(functionToInline, funcNode);

        Node blockorLabel = getBlockForInline(functionToInline, returnVarName, argumentsVarName);

        Node paramList = functionToInline.getChildAtIndex(1);

        Node funcBlockToInline = funcNode;
        if (funcNode.isFunction()) {
            funcBlockToInline = funcNode.getLastChild();
        }

        for (int i = 0; i < paramList.getChildCount(); i++) {
            Node expResNode = new Node(Token.EXPR_RESULT);
            Node assignNode = new Node(Token.ASSIGN);
            expResNode.addChildToBack(assignNode);
            Node varNameNode = paramList.getChildAtIndex(i).cloneNode();
            assignNode.addChildToBack(varNameNode);
            if (callForInline.getChildCount() > i + 1) {
                assignNode.addChildToBack(callForInline.getChildAtIndex(i + 1).cloneTree());
            } else {
                assignNode.addChildToBack(Node.newString(Token.NAME, UNDEFINED));
            }
            exprRes.getParent().addChildBefore(expResNode, exprRes);
        }
        if(arrayLit != null){
            boolean skipFirst = true;
            for(Node arg : callForInline.children()){
                if(skipFirst){
                    skipFirst = false;
                    continue;
                }
                arrayLit.addChildToBack(arg.cloneTree());
            }
        }

        for (Node paramName : paramList.children()) {
            Node varNode = new Node(Token.VAR);
            varNode.addChildToBack(paramName.cloneNode());
            funcBlockToInline.addChildToFront(varNode);
        }

        if (blockorLabel.isLabel()) {
            exprRes.getParent().addChildBefore(blockorLabel, exprRes);
        } else {
            for (Node n : blockorLabel.children()) {
                exprRes.getParent().addChildBefore(n.detachFromParent(), exprRes);
            }
        }

        if (callForInline.getParent().isExprResult()) {
            //call expr res can be removed
            callForInline.getParent().detachFromParent();
        } else {
            //swap call with name
            callForInline.getParent().addChildBefore(returnVarName.cloneTree(), callForInline);
            callForInline.detachFromParent();
        }
    }

    /**
     * replaces all names of declared identifiers in the function which should be inlined with generated names
     * @param funcToInline
     * @param dstFunc
     * @return
     */
    private Node renameNames(Node funcToInline, Node dstFunc) {
        List<Node> nameNodes = getUsedNames(dstFunc);
        Set<String> usedNames = new HashSet<>();
        for (Node name : nameNodes) {
            usedNames.add(name.getString());
        }

        Scope scope = new Scope(null, null);
        NodeTraversal.traverseEs6(compiler, funcToInline, new IdentifierDeclarationExtractor(scope));
        NodeTraversal.traverseEs6(compiler, funcToInline, new IdentifierRefExtractor(scope));

        RandomNameProvider nameProvider = new RandomNameProvider(seed, usedNames, (processOnlyMergingFunctions ? "m" : "") + times);

        scope.renameFunctions(nameProvider);
        scope.renameParameter(nameProvider);
        scope.renameVariables(nameProvider);
        return funcToInline;
    }

    /**
     * returns all name nodes which are used in the scope of given function
     * @param funcNode
     * @return
     */
    private List<Node> getUsedNames(Node funcNode) {
        NameCollectInScope nameCollectInScope = new NameCollectInScope();
        NodeTraversal.traverseEs6(compiler, funcNode, nameCollectInScope);
        return nameCollectInScope.getNames();
    }

    /**
     * creates a variable for the return value of inlined function. the variable deceleration is appended in front of the
     * function body which contains the call
     * @param callForInline
     * @return name node of created variable
     * @throws InlineException
     */
    private Node createReturnVar(Node callForInline) throws InlineException {
        Node funcNode = callForInline;
        while (funcNode != null && !funcNode.isFunction() && !(funcNode.isSyntheticBlock() && funcNode.getParent() == null)) {
            funcNode = funcNode.getParent();
        }
        if (funcNode == null) {
            throw new InlineException();
        }

        Node funcBlock = funcNode;
        if (funcNode.isFunction()) {
            funcBlock = funcNode.getLastChild();
        }
        Node varNode = new Node(Token.VAR);
        Node name = Node.newString(Token.NAME, getUniqueName());
        varNode.addChildToBack(name);

        funcBlock.addChildToFront(varNode);
        return name;
    }

    private String getUniqueName() {
        String name = randomNameProvider.getNextName();
        return name;
    }

    /**
     * transforms function block. returns will be replaced to variable assignments. argument identifier will be replaced to
     * generated identifier. if there are more then one return statement, or the return statement isn't the last statement of function
     * body, the block is converted to a block with label (see convertToLabeldBlock method)
     * @param functionToInline
     * @param returnTarget
     * @param argumentsName
     * @return
     */
    private Node getBlockForInline(Node functionToInline, Node returnTarget, Node argumentsName) {
        Node funcBlock = functionToInline.getLastChild();

        HashSet<Token> tokens = new HashSet<>();
        tokens.add(Token.RETURN);
        CollectTraversal returnCollect = new CollectTraversal(tokens);
        NodeTraversal.traverseEs6(compiler, funcBlock, returnCollect);
        List<Node> returns = returnCollect.getNodes();

        if (argumentsName != null) {
            renameArgument(funcBlock, argumentsName);
        }

        if (returns.isEmpty()) {
            if (returnTarget != null) {
                Node assign = new Node(Token.ASSIGN);
                assign.addChildToBack(returnTarget.cloneTree());
                assign.addChildToBack(Node.newString(Token.NAME, UNDEFINED));
                Node exprRes = new Node(Token.EXPR_RESULT);
                exprRes.addChildToBack(assign);
                funcBlock.addChildToBack(exprRes);
            }
            return funcBlock;
        }

        if (returns.size() == 1 && returns.get(0).equals(funcBlock.getLastChild())) {
            funcBlock.removeChild(returns.get(0));

            if (returnTarget != null) {
                Node assign = new Node(Token.ASSIGN);
                assign.addChildToBack(returnTarget.cloneTree());
                Node rightValue;
                if (returns.get(0).hasChildren()) {
                    rightValue = returns.get(0).getFirstChild().detachFromParent();
                } else {
                    rightValue = Node.newString(Token.NAME, UNDEFINED);
                }
                assign.addChildToBack(rightValue);
                Node exprRes = new Node(Token.EXPR_RESULT);
                exprRes.addChildToBack(assign);

                funcBlock.addChildToBack(exprRes);
            } else {
                if(returns.get(0).hasChildren()){
                    Node rightValue = returns.get(0).getFirstChild().detachFromParent();
                    Node expRes = new Node(Token.EXPR_RESULT);
                    expRes.addChildToBack(rightValue);
                    funcBlock.addChildToBack(expRes);
                }
            }
            return funcBlock;
        }

        return convertToLabeldBlock(funcBlock, returnTarget, returns);
    }

    /**
     * this method replaces arguments name node with generated name node
     * @param funcBlock
     * @param argumentsName
     */
    private void renameArgument(Node funcBlock, Node argumentsName) {
        List<Node> names = getUsedNames(funcBlock);
        for (Node name : names) {
            if (name.getString().equals(ARGUMENTS)) {
                name.getParent().addChildBefore(argumentsName.cloneTree(), name);
                name.detachFromParent();
            }
        }
    }

    /**
     * this method is used if a function with more then one return statement should be inlined. a label will be attached to
     * the function block and a return stattement will be replaced with a assignment and a labled break statement
     * @param funcBlock
     * @param returnTarget
     * @param returns
     * @return
     */
    private Node convertToLabeldBlock(Node funcBlock, Node returnTarget, List<Node> returns) {
        Node blockLabel = generateLabelName();
        Node labeldBlock = new Node(Token.LABEL);
        labeldBlock.addChildToBack(blockLabel);
        labeldBlock.addChildToBack(funcBlock.detachFromParent());

        Node breakPattern = new Node(Token.BREAK);
        breakPattern.addChildToBack(blockLabel.cloneTree());

        for (Node ret : returns) {
            if (returnTarget != null) {
                Node rightValue = ret.hasChildren() ? ret.getFirstChild().detachFromParent() : Node.newString(Token.NAME, UNDEFINED);
                Node assign = new Node(Token.ASSIGN);
                assign.addChildToBack(returnTarget.cloneTree());
                assign.addChildToBack(rightValue);
                Node exprRes = new Node(Token.EXPR_RESULT);
                exprRes.addChildToBack(assign);

                ret.getParent().addChildBefore(exprRes, ret);
            }else{
                if(ret.hasChildren()){
                    Node rightValue = ret.getFirstChild().detachFromParent();
                    Node exprRes = new Node(Token.EXPR_RESULT);
                    exprRes.addChildToBack(rightValue);

                    ret.getParent().addChildBefore(exprRes, ret);
                }
            }

            ret.getParent().addChildBefore(breakPattern.cloneTree(), ret);
            ret.detachFromParent();
        }

        return labeldBlock;
    }

    /**
     * generates a name node which will be utilized as label
     * @return
     */
    private Node generateLabelName() {
        String name;
        do {
            name = BLOCK_LABEL_PREFIX + String.valueOf(random.nextInt(Integer.MAX_VALUE)) + (processOnlyMergingFunctions ? "m" : "") + times;
        } while (usedBlockLabels.contains(name));
        usedBlockLabels.add(name);
        return Node.newString(Token.LABEL_NAME, name);
    }

    /**
     * filters the list of potential inline calls. identifiers which aren't in own function scope, have to be checked to
     * accessibility on target scope. this check perform a lookup from source scope and target scope. if they point to the same
     * decleration, the check passes successfully.
     * also calls which are in context of a a loop head are filtered.
     * if this pass operates in "only merged functions mode", calls which aren't in a merged function block are filtered, too.
     * @param refsNotInScope
     * @param srcScope
     * @param potentialCalls
     */
    private void filterCallsWhichCanBeInlined(List<Node> refsNotInScope, Scope srcScope, List<Node> potentialCalls) {
        List<Node> nodesToRemove = new ArrayList<>();
        for (Node callNode : potentialCalls) {
            Scope dstScope = getScopeForCall(callNode);
            for (Node refNode : refsNotInScope) {
                if (!doNamesRefSameDecl(refNode, srcScope, dstScope)) {
                    nodesToRemove.add(callNode);
                    break;
                }
            }
            if (!isCallInExprResContext(callNode)) {
                nodesToRemove.add(callNode);
            }
            if(processOnlyMergingFunctions && !dstScope.getFunction().getFirstChild().getString().startsWith(FunctionMergingPass.FUNC_PREFIX)){
                nodesToRemove.add(callNode);
            }
        }
        potentialCalls.removeAll(nodesToRemove);
    }

    /**
     * this method checks, if a name node references the same from two given scopes
     * @param refNode
     * @param first
     * @param second
     * @return
     */
    private boolean doNamesRefSameDecl(Node refNode, Scope first, Scope second) {
        Identifier firstDecl = first.getDecleration(refNode);
        Identifier secDecl = second.getDecleration(refNode);
        return firstDecl != null && firstDecl.equals(secDecl);
    }

    /**
     * this method is used to filter calls which are in a loop head or a logical evaluation. this is because of short evaluation
     * syntax which can't be guaranteed to perform right with side effect functions.
     * @param nameForCall
     * @return
     */
    private boolean isCallInExprResContext(Node nameForCall) {
        while (nameForCall != null) {
            if (nameForCall.isExprResult() || nameForCall.isReturn()) {
                return true;
            }
            if(nameForCall.isFor() || nameForCall.isWhile() || nameForCall.isDo() || nameForCall.isSwitch() || nameForCall.isOr() || nameForCall.isAnd() || nameForCall.isHook()){
                return false;
            }
            nameForCall = nameForCall.getParent();
        }
        return false;
    }

    /**
     * returns the scope in which the given call resides.
     * @param callNode
     * @return
     */
    private Scope getScopeForCall(Node callNode) {
        Node functionNode = callNode;
        while (!functionNode.isFunction() && !(functionNode.isSyntheticBlock() && functionNode.getParent() == null)) {
            functionNode = functionNode.getParent();
        }
        return rootScope.getFunctionScope(functionNode);
    }

    /**
     * returns true if no other function is declared in the given function
     * @param function
     * @return
     */
    private boolean isTopLevelFunction(Node function) {
        ContainsNodeTraversal containsFunctionNode = ContainsNodeTraversal.Function();
        NodeTraversal.traverseEs6(compiler, function.getLastChild(), containsFunctionNode);
        return !containsFunctionNode.containsToken();
    }

    /**
     * returns the related scope for given function node
     * @param function
     * @return
     */
    private Scope getScopeForFunction(Node function) {
        return rootScope.getFunctionScope(function);
    }

    /**
     * returns all name nodes which are used in given function and aren't declared in given scope
     * @param function
     * @param scope
     * @return
     */
    private List<Node> getAllNameNodesNotRefToScope(Node function, Scope scope) {
        HashSet<Token> tokens = new HashSet<>();
        tokens.add(Token.NAME);
        CollectTraversal collector = new CollectTraversal(tokens);
        NodeTraversal.traverseEs6(compiler, function.getChildAtIndex(1), collector);
        NodeTraversal.traverseEs6(compiler, function.getChildAtIndex(2), collector);
        List<Node> allNames = collector.getNodes();
        return allNames.stream().filter(node -> !scope.hasDecleration(node)).collect(Collectors.toList());
    }

    /**
     * this ast visitor collects all nodes which are in the given token set
     */
    public class CollectTraversal extends NodeTraversal.AbstractPostOrderCallback {
        private List<Node> nodes;
        private Set<Token> nodesToCollect;

        public CollectTraversal(Set<Token> nodeTypes) {
            nodes = new ArrayList<>();
            nodesToCollect = nodeTypes;
        }

        @Override
        public void visit(NodeTraversal t, Node n, Node parent) {
            if (nodesToCollect.contains(n.getToken())) {
                nodes.add(n);
            }
        }

        /**
         * @return collected nodes after traversal
         */
        public List<Node> getNodes() {
            return nodes;
        }
    }

    /**
     * helper class which encapsulates a function declaration with a call of this function and a flag which inicates the use
     * of arguments identifier
     */
    private class DefCallPair {
        Node def;
        Node call;
        boolean usesArgument;

        public DefCallPair(Node def, Node call, boolean usesArgument) {
            this.def = def;
            this.call = call;
            this.usesArgument = usesArgument;
        }
    }
}
