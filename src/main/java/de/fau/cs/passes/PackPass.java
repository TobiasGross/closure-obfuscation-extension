package de.fau.cs.passes;

import com.google.javascript.jscomp.AbstractCompiler;
import com.google.javascript.jscomp.CodePrinter;
import com.google.javascript.jscomp.CompilerPass;
import com.google.javascript.rhino.Node;
import de.fau.cs.template.CodeProvider;
import de.fau.cs.util.HexNameProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * this pass encrypts the comple source script. at runtime the script will be decrypted and executed with eval. the decryption
 * key is generated from the hash of the decryption stub. Additionally, the stub checks if some native javascript functions
 * are hooked. If there are hooked functions the key is altered, so that the decryption will be fail. Finally a debugger
 * trap method is placed in the stub to hamper dynamic analysis. the decryption stub will be obfuscated with control flow
 * flattening, opaque values, string encryption and identifier renaming.
 * @author Tobias Gross
 */
public class PackPass implements CompilerPass {
    private AbstractCompiler abstractCompiler;

    private Node payload;
    private Node packer;


    private final static int[] keyInit = {18124, 7337, 28163, 53386, 14885, 33490, 31885, 57898, 33259, 47702, 43268, 35196, 49502, 42186, 42147, 1052, 39060};
    private int[] key;

    private List<CompilerPass> passes;

    public PackPass(AbstractCompiler abstractCompiler, long seed) {
        this.abstractCompiler = abstractCompiler;
        passes = new ArrayList<>();
        passes.add(new ControlFlowFlatteningPass(abstractCompiler, null, seed + 1));
        passes.add(new OpaqueValuePass(abstractCompiler, seed + 1));
        passes.add(new StringEncryptionPass(abstractCompiler, seed + 1));
        passes.add(new IdentifierRenamePass(abstractCompiler, new HexNameProvider(seed + 1)));
    }

    @Override
    public void process(Node externs, Node root) {
        payload = root;
        packer = CodeProvider.getSimplePaker(abstractCompiler);

        obfuscatePacker();
        calculateKey();
        String encyptedPayload = encryptPayload();
        addPayload(encyptedPayload);
        swapRoot();
    }

    private void obfuscatePacker() {
        runPasses(packer);
        reorderInstructions(packer);
    }

    /**
     * applies obfuscation to decryption stub
     * @param node
     */
    private void runPasses(Node node) {
        for (CompilerPass pass : passes) {
            pass.process(null, node);
        }
    }

    /**
     * some obfuscation methods leave artifacts. in this case, these artifacts will be placed inside the decryption function
     * instead of outside. i.e. alias array and encoded string and string decryption will be placed inside of decryption function
     * @param node
     */
    private void reorderInstructions(Node node) {
        Node encodedStrings = node.getFirstChild().detachFromParent();
        Node stringKey = node.getFirstChild().detachFromParent();
        Node stringsVar = node.getFirstChild().detachFromParent();
        Node aliasArray = node.getFirstChild().detachFromParent();


        Node funcCall = node.getFirstChild().getFirstChild();
        Node functionDef = funcCall.getFirstChild();
        Node funcBlock = functionDef.getLastChild();
        Node parList = functionDef.getChildAtIndex(1);


        Node keyValue = stringKey.getFirstChild().getFirstChild().detachFromParent();
        Node keyName = stringKey.getFirstChild().detachFromParent();
        parList.addChildToBack(keyName);
        funcCall.addChildToBack(keyValue);

        funcBlock.addChildToFront(aliasArray);
        funcBlock.addChildToFront(stringsVar);
        funcBlock.addChildToFront(encodedStrings);
    }

    /**
     * the key for decryption and encryption are the same. it is generated out of init values and the hash of decryption stub
     * this method will receive the stub as javascript code string out of the ast. then calls the calculate key method
     */
    private void calculateKey() {
        Node packerFunc = packer.getFirstChild().getFirstChild().getFirstChild();
        String packerAsString = getAstAsString(packerFunc);
        calculateKey(packerAsString);
    }

    /**
     * first all whitespaces of script string will be removed. then junks the size of 17 chars are created. these junks are
     * xored with the init key values to generate the final encryption/decryption key.
     * @param packer
     */
    private void calculateKey(String packer) {
        //init key
        key = Arrays.copyOf(keyInit, keyInit.length);
        packer = packer.replaceAll("\\s+", "");
        int startPack = packer.length() / 17;
        while(startPack >= 0){
            int startIndex = startPack * 17;
            int endIndex = startIndex + 17;
            if(endIndex > packer.length()){
                endIndex = packer.length();
            }

            String pack = packer.substring(startIndex, endIndex);
            char[] chars = pack.toCharArray();
            for(int i = 0; i < chars.length; i++){
                key[i] = key[i] ^ chars[i];
            }
            startPack--;
        }
    }

    /**
     * this method takes the modified input script as ast and creates a encrypted javascript code string. the string is
     * encoded with a simple xoring of the decrypted string and the generated key
     * @return
     */
    private String encryptPayload() {
        String payloadAsString = getAstAsString(payload);
        char[] payloadAsChars = payloadAsString.toCharArray();
        for(int i = 0; i < payloadAsChars.length; i++){
            payloadAsChars[i] = (char)(payloadAsChars[i] ^ key[i % key.length]);
        }

        return new String(payloadAsChars);
    }

    /**
     * with help of closure the javascript code string is created out of a syntax tree. Oddly this resulting string differs
     * from the string generated at the end of closure compiler process. to match this, &quot;\\\\x26&quot; needs to replaced with &quot;&amp;&quot;
     * @param node
     * @return
     */
    private String getAstAsString(Node node) {
        CodePrinter.Builder builder = new CodePrinter.Builder(node);
        builder.setPrettyPrint(false);
        builder.setLineBreak(true);
        String code = builder.build();

        //replece same weird encodings
        code = code.replaceAll("\\\\x26", "&");
        return code;
    }

    /**
     * adds the encrypted javascript code and adds it as paremeter to the decyption method call
     * @param payload
     */
    private void addPayload(String payload) {
        Node call = packer.getFirstChild().getFirstChild();

        Node payloadString = Node.newString(payload);
        call.addChildAfter(payloadString, call.getFirstChild());
    }

    private void swapRoot() {
        payload.removeChildren();
        payload.addChildToFront(packer);
    }
}
