package de.fau.cs.passes;

import com.google.javascript.jscomp.AbstractCompiler;
import com.google.javascript.jscomp.CompilerPass;
import com.google.javascript.jscomp.NodeTraversal;
import com.google.javascript.rhino.Node;
import com.google.javascript.rhino.Token;
import de.fau.cs.gen.BasicBlockGenerator;
import de.fau.cs.gen.SwitchTableFlowGenerator;
import de.fau.cs.output.BasicBlockDot;
import de.fau.cs.structures.BasicBlock;
import de.fau.cs.util.BlockHolder;
import de.fau.cs.util.NameProvider;
import de.fau.cs.util.RandomNameProvider;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * This Compiler Pass builds basic block graphs for every JavaScript function. This graph is used to transform control
 * structures (e.g. for, while, if...) to a big switch case construct.
 * @author Tobias Gross
 */
public class ControlFlowFlatteningPass implements CompilerPass {
    private AbstractCompiler compiler;
    private List<Node> functionNodes;
    private List<Node> tryNodes;
    private final PrintStream printStream;
    private final SwitchTableFlowGenerator switchTableFlowGenerator;
    private final Random random;
    private ForInTransformation forInTransformation;

    /**
     *
     * @param compiler used for AST traversal methods
     * @param printStream in case != null, basic block graph is printed to target in graphviz dot format
     * @param seed
     */
    public ControlFlowFlatteningPass(AbstractCompiler compiler, PrintStream printStream, long seed) {
        this.compiler = compiler;
        this.functionNodes = new ArrayList<>();
        this.tryNodes = new ArrayList<>();
        this.printStream = printStream;
        this.switchTableFlowGenerator = new SwitchTableFlowGenerator(seed);
        this.random = new Random(seed);
        this.forInTransformation = new ForInTransformation(seed);
    }

    /**
     * This method is called by the compiler at the assigned time (see CLIRunner.createOptions)
     * @param externs ast that represents extern files
     * @param root ast that represents source files
     */
    @Override
    public void process(Node externs, Node root) {
        BlockHolder.getInstance().clear();

        NodeTraversal.traverseEs6(compiler, root, new RemoveUnnecessaryBlockNodes());
        NodeTraversal.traverseEs6(compiler, root, forInTransformation);
        NodeTraversal.traverseEs6(compiler, root, new FunctionExtractor(functionNodes, tryNodes));

        beginGraph();

        int functionId = 0;
        for(Node function : functionNodes){
            BasicBlockGenerator gen = new BasicBlockGenerator(function.getChildAtIndex(2), random);
            List<BasicBlock> functionBlocks = gen.generateBasicBlocks();
            addFunctionToGraph(function,functionBlocks, functionId);
            if(functionBlocks.size() > 2) {
                switchTableFlowGenerator.restructureFunctionInSwitchCase(function, functionBlocks);
            }
            functionId++;
        }

        for(Node tryNode : tryNodes){
            BasicBlockGenerator gen = new BasicBlockGenerator(tryNode.getFirstChild(), random);
            List<BasicBlock> blocks = gen.generateBasicBlocks();
            if(blocks.size() > 1) {
                switchTableFlowGenerator.restructureTryInSwitchCase(tryNode, blocks);
            }
            functionId++;
        }
        endGraph();

    }

    /**
     * Prints basic block graph for a js function in dot format. Uses a graphviz subgraph cluster as container for each call.
     * @param function used to determine function name to draw as subgraph label
     * @param functionBlocks all basic blocks of function
     * @param functionCounter used to distinguish basic blocks between functions. has to be different for each call
     */
    private void addFunctionToGraph(Node function, List<BasicBlock> functionBlocks, int functionCounter){
        if(printStream != null){
            String functionName;
            if(function.getParent().isName()){
                functionName = function.getParent().getString();
            }else{
                functionName = function.getFirstChild().getString();
            }
            printStream.println("subgraph cluster" + functionCounter +" {");
            printStream.println("label = \"function " + functionName + "\";");

            for(BasicBlock bb : functionBlocks){
                BasicBlockDot.printBasicBlock(printStream, functionCounter, bb);
            }
            printStream.println("}");
        }
    }


    /**
     * Prints Graphviz dot graph start and configure node shape and draw direction.
     */
    private void beginGraph(){
        if(printStream != null){
            printStream.println("digraph BasicBlocks {");
            printStream.println("node [shape=record];");
            printStream.println("rankdir=LR;");
        }
    }

    /**
     * Prints Graphiz graph end
     */
    private void endGraph(){
        if(printStream != null){
            printStream.println("}");
        }
    }

    /**
     * This AST Traversal extracts every function deceleration and try constructs. These nodes are returned in passed Lists.
     * Prepares the functions as append return at the end if doesn't exist. Prepares try block as append assignment of loop flag.
     * These preparations are needed for correct functionality of switch tables (see switch table flow generator)
     */
    private static class FunctionExtractor extends NodeTraversal.AbstractPostOrderCallback{

        private List<Node> functionNodes;
        private List<Node> tryNodes;

        public FunctionExtractor(List<Node> functionNodes, List<Node> tryNodes) {
            this.functionNodes = functionNodes;
            this.tryNodes = tryNodes;
        }

        @Override
        public void visit(NodeTraversal t, Node n, Node parent) {
            if(n.isFunction()){
                Node funcBlock = n.getChildAtIndex(2);
                if(funcBlock != null){
                    if(funcBlock.getLastChild() == null){
                        return;
                    }
                    if(!funcBlock.getLastChild().isReturn()){
                        funcBlock.addChildToBack(new Node(Token.RETURN));
                    }
                }
                functionNodes.add(n);
            }
            if(n.isTry()){
                Node tryBlock = n.getFirstChild();
                tryBlock.addChildToBack(generateFlowBreak());
                tryBlock.addChildToBack(new Node(Token.BREAK));
                tryNodes.add(n);
            }
        }

        /**
         * appends a assignment statement to the end of try block. this assignment will set the while loop flag to false.
         * this will cause the switch table to end. see switch table flow generator.
         * @return
         */
        private Node generateFlowBreak(){
            Node exprRes = new Node(Token.EXPR_RESULT);
            Node assign = new Node(Token.ASSIGN);
            Node varName = Node.newString(Token.NAME, SwitchTableFlowGenerator.BREAK_VAR_NAME);
            Node falseNode = new Node(Token.FALSE);
            assign.addChildToBack(varName);
            assign.addChildToBack(falseNode);
            exprRes.addChildToBack(assign);
            return exprRes;
        }
    }

    /**
     * todo
     * this class should transform a for in loop in a standard for loop. then basic block generation doesn't have to treat
     * the for in case
     */
    private static class ForInTransformation extends NodeTraversal.AbstractPostOrderCallback{
        private final static Node callPattern;
        private final static String PROPERTY_INDEX_POSTFIX = "PROPINDEX";
        private NameProvider nameProvider;

        static{
            callPattern = new Node(Token.CALL);
            Node getProp = new Node(Token.GETPROP);
            getProp.addChildToBack(Node.newString(Token.NAME, "Object"));
            getProp.addChildToBack(Node.newString("keys"));
            callPattern.addChildToBack(getProp);
        }

        public ForInTransformation(long seed) {
            nameProvider = new RandomNameProvider(seed, Collections.EMPTY_SET, PROPERTY_INDEX_POSTFIX);
        }

        @Override
        public void visit(NodeTraversal t, Node n, Node parent) {
            if(n.isFor() && n.getChildCount() == 3){
                Node block = n.getLastChild().detachFromParent();
                Node var = n.getFirstChild().detachFromParent();
                Node target = n.getFirstChild().detachFromParent();
                Node namePattern;

                Node callKeys = callPattern.cloneTree();
                callKeys.addChildToBack(target);

                if(var.isVar()){
                    var.getFirstChild().addChildToBack(callKeys);
                    namePattern = var.getFirstChild();
                }else{
                    namePattern = var;
                    Node exprRes = new Node(Token.EXPR_RESULT);
                    Node assign = new Node(Token.ASSIGN);
                    assign.addChildToBack(namePattern);
                    assign.addChildToBack(callKeys);
                    exprRes.addChildToBack(assign);
                    var = exprRes;
                }

                n.getParent().addChildBefore(var, n);

                Node indexVar = new Node(Token.VAR);
                Node indexName = Node.newString(Token.NAME, nameProvider.getNextName());
                indexVar.addChildToBack(indexName);
                indexName.addChildToBack(Node.newNumber(0));

                Node lower = new Node(Token.LT);
                Node getProp = new Node(Token.GETPROP);
                getProp.addChildToBack(namePattern.cloneNode());
                getProp.addChildToBack(Node.newString("length"));
                lower.addChildToBack(indexName.cloneNode());
                lower.addChildToBack(getProp);

                Node increment = new Node(Token.INC);
                increment.addChildToBack(indexName.cloneNode());

                n.addChildToBack(indexVar);
                n.addChildToBack(lower);
                n.addChildToBack(increment);
                n.addChildToBack(block);

                changePropertyAccess(block, namePattern, indexName);
            }
        }

        void changePropertyAccess(Node block, Node propertiesName, Node indexName){
            for(Node aNode : block.children()){
                changePropertyAccess(aNode, propertiesName, indexName);

                if(aNode.isName() && aNode.getString().equals(propertiesName.getString())){
                    Node acces = new Node(Token.GETELEM);
                    acces.addChildToBack(propertiesName.cloneNode());
                    acces.addChildToBack(indexName.cloneNode());
                    aNode.getParent().addChildBefore(acces, aNode);
                    aNode.detachFromParent();
                }
            }
        }
    }

    private static class RemoveUnnecessaryBlockNodes extends NodeTraversal.AbstractPostOrderCallback{

        @Override
        public void visit(NodeTraversal t, Node n, Node parent) {
            if(n != null && parent != null && n.isBlock() && parent.isBlock()){
                for(Node blockNode : n.children()){
                    Node copy  = blockNode.cloneTree();
                    parent.addChildBefore(copy, n);
                }
                parent.removeChild(n);
            }
        }
    }
}
