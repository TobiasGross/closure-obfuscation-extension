package de.fau.cs;

import com.google.javascript.jscomp.CommandLineRunner;
import com.google.javascript.jscomp.CompilerOptions;
import com.google.javascript.jscomp.CustomPassExecutionTime;
import de.fau.cs.passes.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * This class extends closures CommandLineRunner class. Handles new switches for obfuscation and extends help view.
 *
 * @author Tobias Gross
 */
public class CLIRunner extends CommandLineRunner {
    private boolean renameFunctions = false;
    private boolean flattenCode = false;
    private boolean runTestPass = false;
    private static boolean SHOW_HELP = false;
    private boolean printBasicBlocks = false;
    private boolean extractFunctionNames = false;
    private boolean insertOpaqueValues = false;
    private boolean inlineFunctions = false;
    private boolean mergeFunctions = false;
    private boolean encryptStrings = false;
    private boolean pack = false;
    private boolean addTrap = false;

    /**
     * This constructor sets boolean flags according to custom cli arguments.
     *
     * @param args       stock closure cli arguments
     * @param customArgs custom cli arguments of obfuscation extension
     */
    protected CLIRunner(String[] args, List<String> customArgs, long seed) {
        super(args);
        this.seed = seed;

        customArgs.stream().forEach(arg -> {
            if (arg.equals("--rename_functions")) {
                renameFunctions = true;
            }

            if (arg.equals("--testtest")) {
                runTestPass = true;
            }

            if (arg.equals("--help")) {
                SHOW_HELP = true;
            }

            if (arg.equals("--flatten")) {
                flattenCode = true;
            }

            if (arg.equals("--printBasicBlocks")) {
                printBasicBlocks = true;
            }

            if (arg.equals("--printFunctionNames")) {
                extractFunctionNames = true;
            }

            if(arg.equals("--opaqueValues")) {
                insertOpaqueValues = true;
            }

            if(arg.equals("--inlineFunctions")){
                inlineFunctions = true;
            }
            if(arg.equals("--mergeFunctions")){
                mergeFunctions = true;
            }
            if(arg.equals("--encryptStrings")){
                encryptStrings = true;
            }
            if(arg.equals("--pack")){
                pack = true;
            }
            if(arg.equals("--low")){
                renameFunctions = true;
                encryptStrings = true;
            }

            if(arg.equals("--mediumA")){
                renameFunctions = true;
                encryptStrings = true;
                mergeFunctions = true;
                inlineFunctions = true;
            }

            if(arg.equals("--mediumB")){
                renameFunctions = true;
                encryptStrings = true;
                flattenCode = true;
                insertOpaqueValues = true;
            }

            if(arg.equals("--high")){
                renameFunctions = true;
                encryptStrings = true;
                mergeFunctions = true;
                flattenCode = true;
                insertOpaqueValues = true;
            }

            if(arg.equals("--max")){
                renameFunctions = true;
                encryptStrings = true;
                mergeFunctions = true;
                inlineFunctions = true;
                flattenCode = true;
                insertOpaqueValues = true;
            }

            if(arg.equals("--trap")){
                addTrap = true;
            }

        });
    }

    private long seed;

    private final static String BOLD_PREFIX = "\033[1m";
    private final static String NORMAL_PREFIX = "\033[0m";

    /**
     * Calls createOptions method of CommandLineRunner class, which arrange compiler passes according to cli arguments. Afterwards this method arranges obfuscation compiler passes.
     * This method is called by CommandLineRunner class.
     */
    @Override
    protected CompilerOptions createOptions() {
        CompilerOptions options = super.createOptions();
        //order is important

        if(addTrap){
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new TrapAddPass(getCompiler()));
        }
        if(mergeFunctions){
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new FunctionMergingPass(getCompiler(), seed));
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new FunctionInliningPass(getCompiler(),1, seed,true));
        }

        if(inlineFunctions){
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new FunctionInliningPass(getCompiler(), 1, seed));
        }

        if (flattenCode) {
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new ControlFlowFlatteningPass(getCompiler(), null, seed));
        }
        if(insertOpaqueValues){
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new OpaqueValuePass(getCompiler(), seed));
        }
        if(encryptStrings){
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new StringEncryptionPass(getCompiler(), seed));
        }
        if (renameFunctions) {
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new IdentifierRenamePass(getCompiler(), seed));
        }

        if(encryptStrings || insertOpaqueValues){
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new WrapInFunctionPass());
        }

        if(pack){
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new PackPass(getCompiler(), seed));
        }

        //other passes (not really obfuscation)
        if (runTestPass) {
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new TestPass(getCompiler()));
        }
        if (printBasicBlocks) {
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new ControlFlowFlatteningPass(getCompiler(), System.out, seed));
        }
        if (extractFunctionNames) {
            options.addCustomPass(CustomPassExecutionTime.BEFORE_OPTIMIZATIONS, new FunctionNameExtractionPass(getCompiler()));
        }
        return options;
    }

    /**
     * This method prints help text for obfuscation switches
     */
    public void showCustomHelp() {
        System.out.println(BOLD_PREFIX + "Obfuscation:" + NORMAL_PREFIX);
        System.out.println(" --rename_functions");
    }

    private static String[] removeSeedArg(String[] arg){
        List<String> ret = new ArrayList<>();
        //String[] ret = new String[arg.length -2];
        for(int i = 0; i < arg.length; i++){
            if(arg[i].equals("--seed")){
                i++;
            }else{
                ret.add(arg[i]);
            }
        }

        return ret.toArray(new String[ret.size()]);
    }


    /**
     * Entry Point for calling closure-obfuscation-extension in command line. Divides arguments in stock arguments (original closure) and obfuscation extension arguments.
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        List<String> customArgs = new ArrayList<>();

        Long seed = null;
        for(int i = 0; i < args.length; i++){
            if(args[i].equals("--seed")){
                seed = Long.parseUnsignedLong(args[i + 1], 16);
                break;
            }
        }

        if(seed == null){
            seed = new Random().nextLong();
            System.err.println("INFO - No seed provided. Generated new: " + Long.toUnsignedString(seed, 16));
        }

        boolean filter = false;
        String[] filteredArgs = Arrays.asList(removeSeedArg(args)).stream()
                .filter(arg -> {
                    if (arg.equals("--rename_functions")) {
                        customArgs.add(arg);
                        return false;
                    }

                    if (arg.equals("--flatten")) {
                        customArgs.add(arg);
                        return false;
                    }

                    if (arg.equals("--testtest")) {
                        customArgs.add(arg);
                        return false;
                    }

                    if (arg.equals("--printBasicBlocks")) {
                        customArgs.add(arg);
                        return false;
                    }

                    if (arg.equals("--printFunctionNames")) {
                        customArgs.add(arg);
                        return false;
                    }

                    if(arg.equals("--opaqueValues")) {
                        customArgs.add(arg);
                        return false;
                    }

                    if(arg.equals("--inlineFunctions")){
                        customArgs.add(arg);
                        return false;
                    }
                    if(arg.equals("--mergeFunctions")){
                        customArgs.add(arg);
                        return false;
                    }
                    if(arg.equals("--encryptStrings")){
                        customArgs.add(arg);
                        return false;
                    }
                    if(arg.equals("--pack")){
                        customArgs.add(arg);
                        return false;
                    }

                    if(arg.equals("--trap")){
                        customArgs.add(arg);
                        return false;
                    }

                    if(arg.equals("--low")){
                        customArgs.add(arg);
                        return false;
                    }

                    if(arg.equals("--mediumA")){
                        customArgs.add(arg);
                        return false;
                    }

                    if(arg.equals("--mediumB")){
                        customArgs.add(arg);
                        return false;
                    }

                    if(arg.equals("--max")){
                        customArgs.add(arg);
                        return false;
                    }

                    if (arg.equals("--help")) {
                        customArgs.add(arg);
                    }
                    return true;
                }).toArray(size -> new String[size]);

        if (customArgs.contains("--printBasicBlocks") || customArgs.contains("--printFunctionNames")) {
            filteredArgs = Arrays.copyOf(filteredArgs, filteredArgs.length + 1);
            filteredArgs[filteredArgs.length - 1] = "--checks_only";
        }else{
            filteredArgs = Arrays.copyOf(filteredArgs, filteredArgs.length + 2);
            filteredArgs[filteredArgs.length - 2] = "-O";
            filteredArgs[filteredArgs.length - 1] = "WHITESPACE_ONLY";
        }

        CLIRunner runner = new CLIRunner(filteredArgs, customArgs, seed);
        if (runner.shouldRunCompiler()) {
            runner.run();
        } else {
            if (SHOW_HELP) {
                runner.showCustomHelp();
            } else {
                System.exit(-1);
            }
        }
    }


}
