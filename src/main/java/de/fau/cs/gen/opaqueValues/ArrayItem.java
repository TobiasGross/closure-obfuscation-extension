package de.fau.cs.gen.opaqueValues;

/**
 * abstract item class of alias array. provides methods to check the item type. also a method to determine the index in
 * containing array is provided
 * @author Tobias Gross
 */
public abstract class ArrayItem {
    protected static final int CONST_VALUE = 0;
    protected static final int INVARIANT = 1;
    protected static final int BOGUS_VALUE = 2;

    private AliasArray aliasArray;

    private int initValue;

    int getInitValue() {
        return initValue;
    }

    void setInitValue(int initValue) {
        this.initValue = initValue;
    }

    abstract int type();

    boolean isBogusItem() {
        return type() == BOGUS_VALUE;
    }

    boolean isConstantItem() {
        return type() == CONST_VALUE;
    }

    boolean isInvariantItem() {
        return type() == INVARIANT;
    }

    void setAliasArray(AliasArray aliasArray){
        this.aliasArray = aliasArray;
    }

    int getIndexInArray(){
        return aliasArray.indexOf(this);
    }
}
