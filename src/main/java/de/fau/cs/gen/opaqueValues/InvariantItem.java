package de.fau.cs.gen.opaqueValues;

/**
 * a alias array element which is corresponding to a invariant
 * @author Tobias Gross
 */
public class InvariantItem extends ArrayItem {
    private final Invariant invariant;

    private InvariantItem(Invariant invariant) {
        this.invariant = invariant;
        invariant.add(this);
    }

    /**
     * factory method
     * @param invariant
     * @param initValue
     * @param aliasArray
     * @return
     */
    static InvariantItem createInvariantItem(Invariant invariant, int initValue, AliasArray aliasArray){
        InvariantItem item = new InvariantItem(invariant);
        item.setInitValue(initValue);
        aliasArray.add(item);
        return item;
    }

    @Override
    int type() {
        return INVARIANT;
    }

    @Override
    void setInitValue(int initValue) {
        initValue = findInvariantNear(initValue);
        super.setInitValue(initValue);
    }

    /**
     * finds closest number to given value which holds the corresponding invariant
     * @param value
     * @return
     */
    int findInvariantNear(int value){
        int thatRemainder = value % invariant.getDivisor().getInitValue();
        return value + (invariant.getRemainder().getInitValue() - thatRemainder);
    }
}
