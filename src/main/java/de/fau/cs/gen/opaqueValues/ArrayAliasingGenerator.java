package de.fau.cs.gen.opaqueValues;

import com.google.javascript.rhino.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * this class is responsible for creating alias arrays. some general parameter are set in the static values. so in each
 * generates alias array there are invariants with remainders 1 and 2. next condition is, that all remainders in invariants
 * are smaller then 50. the max divisor size in invariants is 250.
 * @author Tobias Gross
 */
public class ArrayAliasingGenerator {
    private final static Integer[] STATIC_INVARIANT_REMAINDERS = {1,2};
    private final static int REMAINDER_MAX_SIZE = 50;
    private final static int DIVISOR_MAX_SIZE = REMAINDER_MAX_SIZE * 5;
    private final static String ALIAS_ARRAY_NAME = "___AA";

    private final AliasArray aliasArray;
    private final List<Invariant> invariants;
    private final Random random;
    private final TreeGenerator treeGenerator;

    /**
     * the constructor takes some parameter to characterize the generated alias array.
     * first generates the invariants
     * second generates the items related to the invariants
     * third generates the bogus items appropriate to the given rate
     * last, shuffles the items in the alias array
     * @param invariantCount distinguishable invariants. at least 2 and max 25
     * @param invariantItemsPerInvariant the amount of items which are related to a invariant. at least 3
     * @param rateOfBogusItems between 0 and 1
     * @param seed
     */
    public ArrayAliasingGenerator(int invariantCount, int invariantItemsPerInvariant, double rateOfBogusItems, long seed) {
        assert(invariantCount >= STATIC_INVARIANT_REMAINDERS.length);
        assert(invariantCount < REMAINDER_MAX_SIZE / 2);
        assert(rateOfBogusItems <= 1.0);
        assert(invariantItemsPerInvariant > 2);

        aliasArray = new AliasArray();
        invariants = new ArrayList<>();
        random = new Random(seed);
        treeGenerator = new TreeGenerator(ALIAS_ARRAY_NAME);

        generateInvariants(invariantCount);
        generateInvariantItems(invariantItemsPerInvariant);
        generateBogusItems(rateOfBogusItems);
        shuffleArray();
    }

    /**
     *
     * @return a tree which represents the decleration of alias array
     */
    public Node getGlobalArrayInit(){
        return treeGenerator.generateAliasArrayInit(aliasArray);
    }

    /**
     *
     * @param number
     * @return a tree which represents an expression, which constructs the requested value with modulo and summations
     */
    public Node getNumber(long number){
        if(number < 0){
            Node result = generateNumberTree(number * -1);
            return treeGenerator.generateMul(Node.newNumber(-1), result);
        }
        return generateNumberTree(number);
    }

    /**
     * recursive method which chooses the greatest divisor from invariant remainders to build up arithmetic expressions
     * @param number
     * @return
     */
    private  Node generateNumberTree(long number){
        List<Invariant> invariants = new ArrayList<>();
        long currentProduct = 1;
        while(true){
            long maxFactor = number / currentProduct;
            Invariant found = findGreatestInvariant(maxFactor);

            if(found.getRemainder().getInitValue() == 1 && !invariants.isEmpty()){
                break;
            }
            invariants.add(found);
            currentProduct *= (long)found.getRemainder().getInitValue();
        }
        Node ret = generateProductTree(invariants);

        long remainder = number - currentProduct;
        if(remainder != 0){
            Node res = generateNumberTree(remainder);
            return treeGenerator.generateAdd(ret, res);
        }
        return ret;
    }

    /**
     * takes a list of invariants. choose random invariant items to calculate fix remainders. then generates a product over
     * all the remainders
     * @param invariants
     * @return
     */
    private Node generateProductTree(List<Invariant> invariants){
        Node ret = treeGenerator.generateRandomInvariantResolve(invariants.remove(0));
        for(Invariant i : invariants){
            ret = treeGenerator.generateMul(ret, treeGenerator.generateRandomInvariantResolve(i));
        }
        return ret;
    }

    /**
     * chooses a random invariant and creates a always true expression:
     * random_invariant_item % invariant.divisor == invariant.remainder
     * @return equals node
     */
    public Node getTrue(){
        int index = random.nextInt(invariants.size());
        Invariant invariant = invariants.get(index);

        Node left = treeGenerator.generateRandomInvariantResolve(invariant);
        Node right = treeGenerator.generateArrayAccess(invariant.getRemainder().getIndexInArray());
        return treeGenerator.generateEQ(left, right);
    }

    /**
     * chooses two random invariants and creates a always false expression:
     * random_invariant_item1 % invariant1.divisor == invariant2.remainder
     * @return equals node
     */
    public Node getFalse(){
        int index = random.nextInt(invariants.size());
        Invariant firstInvariant = invariants.get(index);
        Invariant secondInvariant;
        do{
            index = random.nextInt(invariants.size());
            secondInvariant = invariants.get(index);
        }while(firstInvariant == secondInvariant);

        Node left = treeGenerator.generateRandomInvariantResolve(firstInvariant);
        Node right = treeGenerator.generateArrayAccess(secondInvariant.getRemainder().getIndexInArray());
        return treeGenerator.generateEQ(left, right);
    }

    /**
     * generates a assignment statement to alias array which alters a random item in array, so that the invariants don't
     * break
     * @return
     */
    public Node getFlux(){
        ArrayItem item = aliasArray.get(random.nextInt(aliasArray.size()));

        if(item.isBogusItem()){
            return generateFakeFlux(item.getIndexInArray());
        }
        if(item.isConstantItem()){
            return generateConstantFlux(item.getIndexInArray(), item.getInitValue());
        }
        if(item.isInvariantItem()){
            return generateInvariantFlux((InvariantItem)item);
        }
        return null;
    }

    /**
     * generates a assignment statement to alias array which alters a random item in array, so that the invariants probably
     * break
     * @return
     */
    public Node getFakeFlux(){
        return generateFakeFlux(random.nextInt(aliasArray.size()));
    }

    /**
     * randomly choose number not equal to zero and creates assignment statement
     * @param arrayIndex
     * @return
     */
    private Node generateFakeFlux(int arrayIndex){
        int val;
        while((val= random.nextInt()) == 0);
        return generateFlux(arrayIndex, val);
    }

    /**
     * generates assignment statement to alias array element with given index which holds the constant condition
     * @param arrayIndex
     * @param constant
     * @return
     */
    private Node generateConstantFlux(int arrayIndex, int constant){
        return generateFlux(arrayIndex, constant);
    }

    /**
     * generates assignment statement to alias array element with given index so that the invariant to which the invariant
     * item is assigned is not violated
     * @param item
     * @return
     */
    private Node generateInvariantFlux(InvariantItem item){
        int val = item.findInvariantNear(random.nextInt(Integer.MAX_VALUE));
        int index = item.getIndexInArray();
        return generateFlux(index, val);
    }

    /**
     * creates the alias array assignment to element with given index and a number calculation with invariants
     * @param index
     * @param value
     * @return assignment node
     */
    private Node generateFlux(int index, int value){
        Node target = treeGenerator.generateArrayAccess(index);
        Node source = getNumber(value);
        return treeGenerator.generateAssignmentResult(target, source);
    }

    /**
     * this method returns the invariant which remainder is the nearest to max but not greater
     * @param max
     * @return
     */
    private Invariant findGreatestInvariant(long max){
        Invariant ret = null;
        for(Invariant i : invariants){
            int invariantValue = i.getRemainder().getInitValue();
            if(ret == null && invariantValue <= max){
                ret = i;
                continue;
            }
            if(invariantValue <= max && ret.getRemainder().getInitValue() < invariantValue){
                ret = i;
            }
        }
        return ret;
    }

    /**
     * randomly generate invariants and the invariants with static choosen remainders
     * @param invariantCount
     */
    private void generateInvariants(int invariantCount){
        int customInvariantCount = invariantCount - STATIC_INVARIANT_REMAINDERS.length;
        //create invariants with static remainders
        for(int rem : STATIC_INVARIANT_REMAINDERS){
            int mod = 0;
            while( rem >= (mod = randomModulu()));
            Invariant i = Invariant.createInvariant(rem, mod, aliasArray, random);
            invariants.add(i);
        }

        //create random invariants
        List<Integer> usedRemainders = new ArrayList<>();
        Collections.addAll(usedRemainders, STATIC_INVARIANT_REMAINDERS);
        while(invariants.size() < invariantCount){
            int rem = 0;
            do{
                rem = randomRemainder();
            }while(usedRemainders.contains(rem));
            usedRemainders.add(rem);

            int mod = 0;
            while( rem >= (mod = randomModulu()));
            Invariant i = Invariant.createInvariant(rem, mod, aliasArray, random);
            invariants.add(i);
        }
    }

    /**
     * iterates over invariants and creates invariant items for each of them
     * @param countPerInvariant
     */
    private void generateInvariantItems(int countPerInvariant){
        for(Invariant i : invariants){
            int count = 0;
            while(count < countPerInvariant){
                InvariantItem.createInvariantItem(i, randomPositiveInt(), aliasArray);
                count++;
            }
        }
    }

    private void generateBogusItems(double rate){
        int count = (int)(aliasArray.size() * rate);
        while(count > 0){
            BogusItem.createBogusItem(randomPositiveInt(), aliasArray);
            count--;
        }
    }

    private void shuffleArray(){
        Collections.shuffle(aliasArray, random);
    }


    private int randomPositiveInt(){
        return random.nextInt(Integer.MAX_VALUE);
    }

    private int randomRemainder(){
        return random.nextInt(REMAINDER_MAX_SIZE + 1);
    }

    private int randomModulu(){
        return random.nextInt(DIVISOR_MAX_SIZE + 1);
    }
}
