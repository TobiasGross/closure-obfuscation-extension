package de.fau.cs.gen.opaqueValues;

import java.util.*;

/**
 * this extendsion of ArrayList modifies the add method, so that a element of that array knows his container
 * @author Tobias Gross
 */
public class AliasArray extends ArrayList<ArrayItem> {
    @Override
    public boolean add(ArrayItem arrayItem) {
        arrayItem.setAliasArray(this);
        return super.add(arrayItem);
    }
}
