package de.fau.cs.gen.opaqueValues;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * a alias array is build up with a certain number of invariants. invariants are created with modulo arithmetic.
 * a invariant consist of a constant item as divisor and a constant as remainder. a list of invariant items are set as
 * follows. a modulo operation of each invariant item with the divisor is equal to the remainder.
 * @author Tobias Gross
 */
public class Invariant {
    private final ConstantItem remainder;
    private final ConstantItem divisor;
    private final List<InvariantItem> invariantItems;
    private final Random random;

    private Invariant(ConstantItem remainder, ConstantItem divisor, Random random) {
        this.remainder = remainder;
        this.divisor = divisor;
        invariantItems = new ArrayList<>();
        this.random = random;
    }

    ConstantItem getRemainder() {
        return remainder;
    }

    ConstantItem getDivisor() {
        return divisor;
    }

    /**
     * factory method. checks precondition and creates all alias array items needed except the invariant items
     * @param remainder
     * @param modulu
     * @param array
     * @param random
     * @return
     */
    static Invariant createInvariant(int remainder, int modulu, AliasArray array, Random random){
        assert(remainder < modulu);
        ConstantItem rem = new ConstantItem(remainder);
        ConstantItem mod = new ConstantItem(modulu);
        Invariant i = new Invariant(rem, mod, random);
        array.add(rem);
        array.add(mod);
        return i;
    }

    boolean add(InvariantItem invariantItem) {
        return invariantItems.add(invariantItem);
    }

    InvariantItem getRandomInvarianItem(){
        int index = random.nextInt(invariantItems.size());
        return invariantItems.get(index);
    }
}
