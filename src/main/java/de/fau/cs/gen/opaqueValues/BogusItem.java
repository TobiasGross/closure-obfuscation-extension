package de.fau.cs.gen.opaqueValues;

/**
 * this class represents a bogus item in the alias array. the value can be changed to any other value with no harm.
 * the only purpose of the bogus items is to obfuscate the invariants in the alias array
 * @author Tobias Gross
 */
public class BogusItem extends ArrayItem {
    @Override
    int type() {
        return BOGUS_VALUE;
    }

    private BogusItem() {
    }

    /**
     * factory method for bogus item
     * @param initValue
     * @param aliasArray
     * @return
     */
    static BogusItem createBogusItem(int initValue, AliasArray aliasArray){
        BogusItem bogusItem = new BogusItem();
        bogusItem.setInitValue(initValue);
        aliasArray.add(bogusItem);
        return bogusItem;
    }
}
