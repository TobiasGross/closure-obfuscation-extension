package de.fau.cs.gen.opaqueValues;

import com.google.javascript.rhino.Node;
import com.google.javascript.rhino.Token;

/**
 * utility class which provides ast fragments for alias array generator
 * @author Tobias Gross
 */
public class TreeGenerator {
    private final Node arrayName;

    TreeGenerator(String arrayName) {
        this.arrayName = Node.newString(Token.NAME, arrayName);
    }

    Node generateAliasArrayInit(AliasArray aliasArray) {
        Node arrayLit = new Node(Token.ARRAYLIT);
        for(ArrayItem item : aliasArray){
            Node number = Node.newNumber(item.getInitValue());
            arrayLit.addChildToBack(number);
        }

        Node name = arrayName.cloneNode();
        name.addChildToBack(arrayLit);
        Node var = new Node(Token.VAR);
        var.addChildToBack(name);
        return var;
    }

    Node generateArrayAccess(int index){
        Node getelem = new Node(Token.GETELEM);
        getelem.addChildToBack(arrayName.cloneNode());
        getelem.addChildToBack(Node.newNumber(index));
        return getelem;
    }

    Node generateAdd(Node first, Node second){
        Node add = new Node(Token.ADD);
        add.addChildToBack(first);
        add.addChildToBack(second);
        return add;
    }

    Node generateMul(Node first, Node second){
        Node add = new Node(Token.MUL);
        add.addChildToBack(first);
        add.addChildToBack(second);
        return add;
    }

    Node generateRandomInvariantResolve(Invariant i){
        ArrayItem item = i.getRandomInvarianItem();
        ArrayItem mod = i.getDivisor();

        Node leftSide = generateArrayAccess(item.getIndexInArray());
        Node rightSide = generateArrayAccess(mod.getIndexInArray());

        Node modNode = new Node(Token.MOD);
        modNode.addChildToBack(leftSide);
        modNode.addChildToBack(rightSide);
        return modNode;
    }

    Node generateEQ(Node left, Node right){
        Node eq = new Node(Token.EQ);
        eq.addChildToBack(left);
        eq.addChildToBack(right);
        return eq;
    }

    Node generateAssignmentResult(Node left, Node right){
        Node exprRes = new Node(Token.EXPR_RESULT);
        Node assignment = new Node(Token.ASSIGN);

        exprRes.addChildToBack(assignment);
        assignment.addChildToBack(left);
        assignment.addChildToBack(right);

        return exprRes;
    }
}
