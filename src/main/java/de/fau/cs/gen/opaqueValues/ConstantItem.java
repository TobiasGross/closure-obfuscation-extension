package de.fau.cs.gen.opaqueValues;

/**
 * const items in alias array are used to create invariants and to create transformations to items which afterwards still
 * hold the invariants. const items are only on the left side of asignment statement if the right side evaluates to the
 * constant init value. such a transformation has only obfuscation purpose.
 * @author Tobias Gross
 */
public class ConstantItem extends ArrayItem {
    ConstantItem(int value) {
        super.setInitValue(value);
    }

    @Override
    int type() {
        return CONST_VALUE;
    }

    @Override
    void setInitValue(int initValue) {
        return;
    }
}
