package de.fau.cs.gen;

/**
 *This Exception is thrown if a exception occurs upon basic block generation. helps to relate bugs in basic block generator
 * to javascript source code constructs
 * @author Tobias Gross
 */
public class BasicBlockGeneratorException extends RuntimeException{
    public BasicBlockGeneratorException(String message, Throwable cause) {
        super(message, cause);
    }
}
