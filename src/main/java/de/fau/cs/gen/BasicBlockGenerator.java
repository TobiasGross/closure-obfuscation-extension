package de.fau.cs.gen;

import com.google.javascript.rhino.Node;
import com.google.javascript.rhino.Token;
import de.fau.cs.structures.BasicBlock;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class takes abstract syntax trees of functions and generates a basic block notation. A basic block insists of
 * statements which are executed sequential. At the end of a basic block is a jump. This jump can either be unconditional
 * or conditional. The basic block notation is influenced by control structures like for/while loops and if else constructs.
 * @author Tobias Gross
 */
public class BasicBlockGenerator {
    private List<BasicBlock> functionBlocks;
    private Node functionBlockNode;

    private List<JumpWrapper> breakBlocks;
    private List<JumpWrapper> continueBlocks;

    private BasicBlock functionDefBlock;

    private final Random random;

    private  int randomIdMin = 0;
    private static final int RANDOM_INTERVAL_DISTANCE = 1000;
    private static final int VALUES_PER_INIT = 50;
    private final List<Integer> ids;

    private final static int INIT_NOP_BLOCK_SET_SIZE = 20;

    /**
     * Initializes basic block generator with a function represented by Node.
     * @param functionBlockNode
     * @param random
     */
    public BasicBlockGenerator(Node functionBlockNode, Random random) {
        functionBlocks = new ArrayList<>();
        breakBlocks = new ArrayList<>();
        continueBlocks = new ArrayList<>();
        this.functionBlockNode = functionBlockNode;
        functionDefBlock = new BasicBlock(-1);
        this.random = random;
        this.ids = new ArrayList<>();
    }

    /**
     * only public method of basic block generator
     * @return list of basic blocks of the function
     */
    public List<BasicBlock> generateBasicBlocks() {
        generateBasicBlocks(functionBlockNode);
        removeEmptyBlocks();
        functionBlocks.add(0, functionDefBlock);
        return functionBlocks;
    }

    /**
     * this method is used to remove empty basic blocks. these blocks arise by this construction method implemented by this class.
     * empty blocks insists only of conditional/unconditional jumps. No statements included.
     */
    private void removeEmptyBlocks() {
        Set<BasicBlock> nopBlocks = new HashSet<>(INIT_NOP_BLOCK_SET_SIZE);
        boolean repeat = true;
        while (repeat) {
            repeat = false;
            for (BasicBlock bb : functionBlocks) {
                if (bb.getJMP() != null) {
                    //next block has no statements and a unconditional jump => Empty Block
                    if (bb.getJMP().getStatements().isEmpty() && bb.getJMP().getJMP() != null) {
                        //empty block to set
                        nopBlocks.add(bb.getJMP());
                        repeat = true;

                        //redirect jump, that empty block is skiped
                        bb.setJMP(bb.getJMP().getJMP());
                    }
                }

                if (bb.getTrueJMP() != null) {
                    //block jumped on true has no statements and a unconditional jump => Empty Block
                    if (bb.getTrueJMP().getStatements().isEmpty() && bb.getTrueJMP().getJMP() != null) {
                        //empty block to set
                        nopBlocks.add(bb.getTrueJMP());
                        repeat = true;

                        //redirect jump, that empty block is skiped
                        bb.setTrueJMP(bb.getTrueJMP().getJMP());
                    }
                    //block jumped on false has no statements and a unconditional jump => Empty Block
                    if (bb.getFalseJMP().getStatements().isEmpty() && bb.getFalseJMP().getJMP() != null) {
                        //empty block to set
                        nopBlocks.add(bb.getFalseJMP());
                        repeat = true;

                        //redirect jump, that empty block is skiped
                        bb.setFalseJMP(bb.getFalseJMP().getJMP());
                    }
                }
            }
        }

        //remove empty blocks from list
        functionBlocks.removeAll(nopBlocks);
    }

    private void wireContinuesBreaks(String assignedLabel, BasicBlock continueTarget, BasicBlock breakTarget){
        if(continueTarget != null) {
            wireJumps(assignedLabel, continueBlocks, continueTarget);
        }
        if(breakTarget != null){
            wireJumps(assignedLabel, breakBlocks, breakTarget);
        }
    }

    private void wireJumps(String assignedLabel, List<JumpWrapper> jumpWrappers, BasicBlock target){
        List<JumpWrapper> assignedJumps = jumpWrappers.stream()
                .filter(jump -> jump.targetLabel == null || jump.targetLabel.equals(assignedLabel))
                .collect(Collectors.toList());
        for (JumpWrapper jumpWrapper : assignedJumps) {
            jumpWrapper.jumpSource.setJMP(target);
        }
        jumpWrappers.removeAll(assignedJumps);
    }

    private void wireLabeledBreaks(String assignedLabel, BasicBlock breakTarget){
        List<JumpWrapper> assignedJumps = breakBlocks.stream()
                .filter(jump -> jump.targetLabel != null && jump.targetLabel.equals(assignedLabel))
                .collect(Collectors.toList());
        for (JumpWrapper jumpWrapper : assignedJumps) {
            jumpWrapper.jumpSource.setJMP(breakTarget);
        }
        breakBlocks.removeAll(assignedJumps);
    }

    /**
     * This method handles while statements in the abstract syntax tree and generates basic blocks accordingly.
     * @param whileSTMT node which represent while statement
     * @param actualBasicBlock this block is completed with jumps according to the while statement
     * @param assignedLabel
     * @return a basic block which is targeted for jumping at the end of loop
     */
    private BasicBlock processWhileSTMT(Node whileSTMT, BasicBlock actualBasicBlock, String assignedLabel) {
        BasicBlock loopCondition;
        if (actualBasicBlock.getStatements().isEmpty()) {
            loopCondition = actualBasicBlock;
        } else {
            loopCondition = generateBB();
            actualBasicBlock.setJMP(loopCondition);
        }
        loopCondition.setCondition(whileSTMT.getFirstChild());


        BasicBlockPair whileBlockResult = generateBasicBlocks(whileSTMT.getSecondChild());
        whileBlockResult.end.setJMP(loopCondition);

        loopCondition.setTrueJMP(whileBlockResult.start);

        //new BB after while Block
        actualBasicBlock = generateBB();
        loopCondition.setFalseJMP(actualBasicBlock);


        //handle breaks and continues
        wireContinuesBreaks(assignedLabel, loopCondition, actualBasicBlock);

        return actualBasicBlock;
    }

    /**
     * This method handles if else statements in the abstract syntax tree and generates basic blocks accordingly.
     * @param ifSTMT node which represent if statement
     * @param actualBasicBlock this block is completed with jumps according to the if statement
     * @param assignedLabel
     * @return a basic block which is targeted for jumping at the end of if/else blocks
     */
    private BasicBlock processIfSTMT(Node ifSTMT, BasicBlock actualBasicBlock, String assignedLabel) {
        //if-else-case
        if (ifSTMT.getChildCount() == 3) {
            actualBasicBlock.setCondition(ifSTMT.getFirstChild());

            BasicBlockPair trueBlockPair = generateBasicBlocks(ifSTMT.getSecondChild());
            BasicBlockPair falseBlockPair = generateBasicBlocks(ifSTMT.getChildAtIndex(2));

            actualBasicBlock.setTrueJMP(trueBlockPair.start);
            actualBasicBlock.setFalseJMP(falseBlockPair.start);

            actualBasicBlock = generateBB();
            trueBlockPair.end.setJMP(actualBasicBlock);
            falseBlockPair.end.setJMP(actualBasicBlock);
        }
        //if-case
        else {
            actualBasicBlock.setCondition(ifSTMT.getFirstChild());

            BasicBlockPair trueBlockPair = generateBasicBlocks(ifSTMT.getSecondChild());

            actualBasicBlock.setTrueJMP(trueBlockPair.start);

            BasicBlock nextBlock = generateBB();
            actualBasicBlock.setFalseJMP(nextBlock);
            trueBlockPair.end.setJMP(nextBlock);

            actualBasicBlock = nextBlock;
        }

        //handle labeled break statements
        wireLabeledBreaks(assignedLabel, actualBasicBlock);

        return actualBasicBlock;
    }

    /**
     * This method handles for statements in the abstract syntax tree and generates basic blocks accordingly.
     * @param forSTMT node which represent for statement
     * @param actualBasicBlock this block is completed with jumps according to the for statement
     * @return a basic block which is targeted for jumping if loop condition isn't true
     */
    private BasicBlock processForSTMT(Node forSTMT, BasicBlock actualBasicBlock, String assignedLabel) {
        Node init = forSTMT.getFirstChild();
        Node loopCondition = forSTMT.getSecondChild();
        Node afterthoughtWithoutExprResWrap = forSTMT.getChildAtIndex(2).cloneTree();
        Node afterthought = new Node(Token.EXPR_RESULT);
        afterthought.addChildToFront(afterthoughtWithoutExprResWrap);
        Node loopBlock = forSTMT.getChildAtIndex(3);

        BasicBlock nextBlock = generateBB();

        if (!init.isEmpty()) {
            init.detachFromParent();
            if (init.isVar()) {
                actualBasicBlock.addStatement(init);
            } else {
                Node exprRes = new Node(Token.EXPR_RESULT);
                exprRes.addChildToFront(init);
                actualBasicBlock.addStatement(exprRes);
            }
        }

        actualBasicBlock.setCondition(loopCondition.cloneTree());
        actualBasicBlock.setFalseJMP(nextBlock);

        BasicBlockPair loopPair = generateBasicBlocks(loopBlock);

        actualBasicBlock.setTrueJMP(loopPair.start);

        BasicBlock loopConditionBlock = generateBB();
        loopPair.end.setJMP(loopConditionBlock);

        if (!afterthought.isEmpty()) {
            loopConditionBlock.addStatement(afterthought);
        }
        loopConditionBlock.setCondition(loopCondition);
        loopConditionBlock.setTrueJMP(loopPair.start);
        loopConditionBlock.setFalseJMP(nextBlock);

        //handle breaks and continues
        wireContinuesBreaks(assignedLabel, loopConditionBlock, nextBlock);

        return nextBlock;
    }

    /**
     * This method handles do statements in the abstract syntax tree and generates basic blocks accordingly.
     * @param doSTMT node which represent do statement
     * @param actualBasicBlock this block is completed with a jumps according to the do statement
     * @param assignedLabel
     * @return a basic block which is targeted for jumping if loop condition isn't true
     */
    private BasicBlock processDoSTMT(Node doSTMT, BasicBlock actualBasicBlock, String assignedLabel) {
        BasicBlock loopCondition = generateBB();
        loopCondition.setCondition(doSTMT.getSecondChild());
        BasicBlock nextBlock = generateBB();

        BasicBlockPair doBlockResult = generateBasicBlocks(doSTMT.getFirstChild());

        actualBasicBlock.setJMP(doBlockResult.start);

        doBlockResult.end.setJMP(loopCondition);

        loopCondition.setTrueJMP(doBlockResult.start);
        loopCondition.setFalseJMP(nextBlock);

        //handle breaks and continues
        wireContinuesBreaks(assignedLabel, loopCondition, nextBlock);

        return nextBlock;
    }

    /**
     * This method handles switch statements in the abstract syntax tree and generates basic blocks accordingly.
     * @param switchSTMT node which represent switch statement
     * @param actualBasicBlock this block is completed with a jumps according to the switch statement
     * @return a basic block which is targeted for jumping at the end of case blocks
     */
    private BasicBlock processSwitchSTMT(Node switchSTMT, BasicBlock actualBasicBlock, String assignedLabel) {
        Node varToCheck = switchSTMT.getFirstChild();
        varToCheck.detachFromParent();

        BasicBlock lastBlock = actualBasicBlock;
        BasicBlock blockToJMPNext = null;

        Node defaultCase = null;
        if (switchSTMT.getLastChild().isDefaultCase()) {
            defaultCase = switchSTMT.getLastChild();
            defaultCase.detachFromParent();
        }

        for (Node caseNode : switchSTMT.children()) {
            Node condition = caseNode.getFirstChild();
            Node caseBlock = caseNode.getSecondChild();
            condition.detachFromParent();

            Node eq = new Node(Token.SHEQ);
            eq.addChildToFront(varToCheck.cloneTree());
            eq.addChildToBack(condition);

            BasicBlock bb = generateBB();
            bb.setCondition(eq);

            lastBlock.setFalseJMP(bb);


            BasicBlockPair bbp = generateBasicBlocks(caseBlock);
            bb.setTrueJMP(bbp.start);
            if (blockToJMPNext != null) {
                blockToJMPNext.setJMP(bbp.start);
            }

            lastBlock = bb;

            blockToJMPNext = bbp.end;
        }

        BasicBlock nextBlock = generateBB();

        if (defaultCase != null) {
            BasicBlockPair bbp = generateBasicBlocks(defaultCase.getFirstChild());
            lastBlock.setFalseJMP(bbp.start);
            blockToJMPNext.setJMP(bbp.start);
            bbp.end.setJMP(nextBlock);
        } else {
            lastBlock.setFalseJMP(nextBlock);
            blockToJMPNext.setJMP(nextBlock);
        }

        //first jump fix
        BasicBlock toJMP = actualBasicBlock.getFalseJMP();
        actualBasicBlock.setFalseJMP(null);
        actualBasicBlock.setJMP(toJMP);

        //fix breaks
        wireContinuesBreaks(assignedLabel, null, nextBlock);

        return nextBlock;
    }

    private BasicBlock processBlockSTMT(Node blockSTMT, BasicBlock actualBasicBlock, String assignedLabel){
        BasicBlockPair bbp = generateBasicBlocks(blockSTMT);
        actualBasicBlock.setJMP(bbp.start);
        BasicBlock next = generateBB();
        bbp.end.setJMP(next);
        wireLabeledBreaks(assignedLabel, next);
        return next;
    }

    /**
     * recursive method which visits the abstract syntax tree nodes supported by the process* methods. Adds statements to
     * basic blocks which the control flow is non depending. Statements which define the control flow are handled by the
     * process* methods except break and continue.
     * @param block a block representing function body, true/fale block of if construct, loop body etc.
     * @return start and end basic block of one recursive run
     */
    private BasicBlockPair generateBasicBlocks(Node block) {
        BasicBlock firstBasicBlock = generateBB();
        BasicBlock actualBasicBlock = firstBasicBlock;

        if (block.getFirstChild() == null) {
            return new BasicBlockPair(firstBasicBlock, firstBasicBlock);
        }

        for (Node statement : block.getFirstChild().siblings()) {
            try {
                String labelName = null;
                if(statement.isLabel()){
                    labelName = statement.getFirstChild().getString();
                    statement = statement.getSecondChild();
                }

                if (statement.isWhile()) {
                    actualBasicBlock = processWhileSTMT(statement, actualBasicBlock, labelName);
                } else if (statement.isIf()) {
                    actualBasicBlock = processIfSTMT(statement, actualBasicBlock, labelName);
                    //forloop with init, condition and afterthought
                } else if (statement.isFor() && statement.getChildCount() == 4) {
                    actualBasicBlock = processForSTMT(statement, actualBasicBlock, labelName);
                    //itertator forloop (for ... in ...)
                } else if (statement.isFor() && statement.getChildCount() == 3) {
                    //// TODO: 16.10.16 transform iteratorloop to forloop in ControlFlowFlatteningPass => then this exceptional handling is obsolet
                    actualBasicBlock.addStatement(statement);
                } else if (statement.isDo()) {
                    actualBasicBlock = processDoSTMT(statement, actualBasicBlock, labelName);
                } else if (statement.isSwitch()) {
                    actualBasicBlock = processSwitchSTMT(statement, actualBasicBlock, labelName);
                } else if (statement.isContinue()) {
                    String targetLabel = null;
                    if(statement.hasChildren() && statement.getFirstChild().isLabelName()){
                        targetLabel = statement.getFirstChild().getString();
                    }
                    continueBlocks.add(new JumpWrapper(actualBasicBlock, targetLabel));
                    return new BasicBlockPair(firstBasicBlock, actualBasicBlock);
                } else if (statement.isBreak()) {
                    String targetLabel = null;
                    if(statement.hasChildren() && statement.getFirstChild().isLabelName()){
                        targetLabel = statement.getFirstChild().getString();
                    }
                    breakBlocks.add(new JumpWrapper(actualBasicBlock, targetLabel));
                    return new BasicBlockPair(firstBasicBlock, actualBasicBlock);
                } else if (statement.isFunction()){
                    //bring funciton definitions to front of functionblock
                    functionDefBlock.addStatement(statement);
                } else if (statement.isBlock()){
                    actualBasicBlock = processBlockSTMT(statement, actualBasicBlock, labelName);
                } else if (statement.isTry()){
                    actualBasicBlock.addStatement(statement);
                    findFunctionDefNodes(statement);
                }
                else {
                    findFunctionDefNodes(statement);
                    actualBasicBlock.addStatement(statement);
                }
            } catch (Exception e) {
                String exceptionText = "Exception while parsing " + statement.getSourceFileName() + " line " + statement.getLineno() + ", token: " + statement.getToken().toString();
                BasicBlockGeneratorException ex = new BasicBlockGeneratorException(exceptionText, e);
                throw ex;
            }
        }

        return new BasicBlockPair(firstBasicBlock, actualBasicBlock);
    }

    private void findFunctionDefNodes(Node node){
        if(node.isFunction()){
            functionDefBlock.addStatement(node);
        }else{
            for(Node n : node.children()){
                findFunctionDefNodes(n);
            }
        }
    }

    /**
     * generates a basic block object with a unique id within a function. all basic blocks are held in a list.
     * @return
     */
    private BasicBlock generateBB() {
        if(ids.isEmpty()){
            initIDs();
        }
        BasicBlock basicBlock = new BasicBlock(ids.remove(0));
        functionBlocks.add(basicBlock);
        return basicBlock;
    }

    private void initIDs(){
        int count = 0;
        while(count < VALUES_PER_INIT){
            int value = randomIdMin +  random.nextInt(RANDOM_INTERVAL_DISTANCE);
            if(ids.contains(value)){
                continue;
            }
            ids.add(value);
            count++;
        }
        randomIdMin += RANDOM_INTERVAL_DISTANCE;
    }

    /**
     * helper structure wich is used by recursive  ast visit method generateBasicBlocks.
     */
    private class BasicBlockPair {
        public BasicBlock start;
        public BasicBlock end;

        public BasicBlockPair(BasicBlock start, BasicBlock end) {
            this.start = start;
            this.end = end;
        }
    }

    private class JumpWrapper {
        public BasicBlock jumpSource;
        public String targetLabel;

        public JumpWrapper(BasicBlock jumpSource, String targetLabel) {
            this.jumpSource = jumpSource;
            this.targetLabel = targetLabel;
        }
    }
}
