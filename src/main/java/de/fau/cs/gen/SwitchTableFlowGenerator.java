package de.fau.cs.gen;

import com.google.javascript.rhino.Node;
import com.google.javascript.rhino.Token;
import de.fau.cs.structures.BasicBlock;
import de.fau.cs.util.BlockHolder;

import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * this class restructures statements of a function to a great switch case construct wrapped in a endless while loop.
 * each case of the switch statement represents a basic block. jumps between blocks are represented by a variable which
 * names the next case block to be executed. at the end of a case block this variable is set accordingly.
 * @author Tobias Gross
 */
public class SwitchTableFlowGenerator {
    private final static String PC_NAME = "___pc";
    public final static String BREAK_VAR_NAME = "___RUN";
    private final Random random;
    private long[] caseNumbers;

    private BlockHolder blockHolder;

    /**
     * initializes the switch table flow generator.
     * @param seed ensures that the output is reproducible
     */
    public SwitchTableFlowGenerator(long seed) {
        this.random = new Random(seed);
        blockHolder = BlockHolder.getInstance();
    }

    /**
     * restructures a function
     * @param function node that represent function in ast
     * @param basicBlocks basic blocks of this function
     */
    public void restructureFunctionInSwitchCase(Node function, List<BasicBlock> basicBlocks){
        BasicBlock functionDefBlock = basicBlocks.remove(0);
        initCaseNumbers(basicBlocks);
        Node switchNode = createSwitchTableSkeletonForFunction(function, basicBlocks.get(0), functionDefBlock);

        Collections.shuffle(basicBlocks, random);
        for(BasicBlock basicBlock : basicBlocks){
            transformBlockToCase(switchNode, basicBlock);
        }
    }

    /**
     * restuctures a try block. try blocks need to be treated apart from functions because of catch scope. (basic blocks
     * of try body can't be handled together with other basic blocks in one switch table)
     * @param tryNode node that represents try in ast
     * @param basicBlocks basic blocks of the try block
     */
    public void restructureTryInSwitchCase(Node tryNode, List<BasicBlock> basicBlocks){
        //remove function def block which doesn't exist in try context
        basicBlocks.remove(0);
        initCaseNumbers(basicBlocks);

        Node switchNode = createSwitchTableSkeletonForTry(tryNode, basicBlocks.get(0));

        Collections.shuffle(basicBlocks, random);
        for(BasicBlock basicBlock : basicBlocks){
            transformBlockToCase(switchNode, basicBlock);
        }
    }

    /**
     * all basic block ids are collected in front of transformation. this is required to construct opaque jumps to false
     * targets
     * @param basicBlocks
     */
    private void initCaseNumbers(List<BasicBlock> basicBlocks){
        caseNumbers = new long[basicBlocks.size()];
        int index = 0;
        for(BasicBlock bb : basicBlocks){
            caseNumbers[index] = bb.getId();
            index++;
        }
    }

    /**
     *
     * @return a random basic block id
     */
    private long getRandomCaseNumber(){
        return caseNumbers[random.nextInt(caseNumbers.length)];
    }

    /**
     * generates a case construct out of a basic block
     * @param switchNode case node is attached to this switch node
     * @param basicBlock this basic block is transformed to the case node
     */
    private void transformBlockToCase(Node switchNode, BasicBlock basicBlock){
        Node caseNode = new Node(Token.CASE);
        Node number = Node.newNumber(basicBlock.getId());
        caseNode.addChildToBack(number);
        switchNode.addChildToBack(caseNode);

        Node block = new Node(Token.BLOCK);
        block.setIsSyntheticBlock(true);
        caseNode.addChildToBack(block);

        for(Node statement : basicBlock.getStatements()){
            if(statement.getParent() != null) {
                statement.detachFromParent();
            }
            block.addChildToBack(statement);
        }

        Node ifStmt = createIfConstruct(basicBlock);
        if(ifStmt != null) {
            block.addChildToBack(ifStmt);
            block.addChildToBack(new Node(Token.BREAK));
        }
    }

    /**
     * this method transform the jump condition of a basic block in a if construct. on a unconditional jump a opaque statement
     * is created which points to the unconditional target and a random basic block.
     * @param basicBlock conditional/unconditional jump ist used to create if node
     * @return if construct represented by node object
     */
    private Node createIfConstruct(BasicBlock basicBlock){
        if(basicBlock.getJMP() == null && basicBlock.getTrueJMP() == null){
            return null;
        }

        Node ifSTMT = new Node(Token.IF);
        Node trueBlock = new Node(Token.BLOCK);
        Node falseBlock = new Node(Token.BLOCK);
        ifSTMT.addChildToBack(trueBlock);
        ifSTMT.addChildToBack(falseBlock);

        //unconditional jump case
        if(basicBlock.getJMP() != null){
            Node rightAssign = createAssignToPC(basicBlock.getJMP().getId());
            Node fakeAssign = createAssignToPC(getRandomCaseNumber());

            if(shouldJumpOnTrue()){
                ifSTMT.addChildToFront(new Node(Token.TRUE));
                trueBlock.addChildToBack(rightAssign);
                falseBlock.addChildToBack(fakeAssign);

                blockHolder.addBlockForFlux(trueBlock);
                blockHolder.addBlockForFakeFlux(falseBlock);
            }else{
                ifSTMT.addChildToFront(new Node(Token.FALSE));
                trueBlock.addChildToBack(fakeAssign);
                falseBlock.addChildToBack(rightAssign);

                blockHolder.addBlockForFlux(falseBlock);
                blockHolder.addBlockForFakeFlux(trueBlock);
            }
        }

        //conditional jump case
        if(basicBlock.getTrueJMP() != null){
            Node condTrueAssign = createAssignToPC(basicBlock.getTrueJMP().getId());
            Node condFalseAssign = createAssignToPC(basicBlock.getFalseJMP().getId());

            Node condition = basicBlock.getCondition();
            if(condition.getParent() != null) {
                condition.detachFromParent();
            }
            ifSTMT.addChildToFront(condition);
            trueBlock.addChildToBack(condTrueAssign);
            falseBlock.addChildToBack(condFalseAssign);

            blockHolder.addBlockForFlux(trueBlock);
            blockHolder.addBlockForFlux(falseBlock);

        }
        return ifSTMT;
    }

    /**
     * creates a assign statement. used to assign next basic block id to pc
     * @param blockId target basic block id
     * @return assign node
     */
    private Node createAssignToPC(long blockId){
        Node assign = new Node(Token.ASSIGN);
        Node number = Node.newNumber(blockId);
        Node name = Node.newString(Token.NAME, PC_NAME);
        assign.addChildToBack(name);
        assign.addChildToBack(number);
        Node exprRes = new Node(Token.EXPR_RESULT);
        exprRes.addChildToBack(assign);
        return exprRes;
    }

    /**
     * used on unconditional jump to decide if jump is done on true or false branch
     * @return
     */
    private boolean shouldJumpOnTrue(){
        return random.nextBoolean();
    }

    /**
     * creates the static code constructs of the switch table construct. this method is used for function transforming
     * @param function function node which is transformed
     * @param firstBasicBlock the first basic block of the function. used to determine the initial value of pc
     * @param functionDefBasicBlock
     * @return
     */
    private Node createSwitchTableSkeletonForFunction(Node function, BasicBlock firstBasicBlock, BasicBlock functionDefBasicBlock){
        function.getLastChild().detachFromParent();
        Node newFunctionBlock = new Node(Token.BLOCK);
        function.addChildToBack(newFunctionBlock);

        addFunctionDefsToBlock(newFunctionBlock, functionDefBasicBlock);

        Node pcInit = createPCInit(firstBasicBlock.getId());
        newFunctionBlock.addChildToBack(pcInit);

        Node switchNode = createSwitch();
        Node whileNode = createWhile(switchNode, null);
        newFunctionBlock.addChildToBack(whileNode);

        return switchNode;
    }

    /**
     * brings function declerations in front of covering function block. this is to prevent bugs which occur if function
     * declerations are in case blocks of a switch statement
     * @param functionBlock
     * @param functionDefBasicBlock
     */
    private void addFunctionDefsToBlock(Node functionBlock, BasicBlock functionDefBasicBlock){
        for(Node functionDef : functionDefBasicBlock.getStatements()){
            if(functionDef.getFirstChild().getString().equals("")){
                continue;
            }
            if(!functionDef.getParent().isBlock()){
                Node call = functionDef.getParent();
                call.addChildBefore(functionDef.getFirstChild().cloneNode(), functionDef);
            }
            functionDef.detachFromParent();
            functionBlock.addChildToFront(functionDef);
        }
    }

    /**
     * creates the static code constructs of the switch table construct. this method is used for try transforming
     * @param tryNode try node which is transformed
     * @param firstBasicBlock the first basic block of the try block. used to determine the initial value of pc
     * @return
     */
    private Node createSwitchTableSkeletonForTry(Node tryNode, BasicBlock firstBasicBlock){
        tryNode.getFirstChild().detachFromParent();
        Node newTryBlock = new Node(Token.BLOCK);
        tryNode.addChildToFront(newTryBlock);

        Node pcInit = createPCInit(firstBasicBlock.getId());
        newTryBlock.addChildToBack(pcInit);

        Node breakVarInit = createBreakVarInit();
        newTryBlock.addChildToBack(breakVarInit);

        Node switchNode = createSwitch();
        Node whileNode = createWhile(switchNode, Node.newString(Token.NAME, BREAK_VAR_NAME));
        newTryBlock.addChildToBack(whileNode);

        return switchNode;
    }

    /**
     * in case of try catch construct the endless loop method does not work, because a return statement can't be utilized.
     * this results, because try constructs can be integrated in functions. this will build a switch table in a switch table.
     * return will jump out of both tables but its aimed to only jump out of inner table. so instead of return statement we
     * set a variable which indicates to proceed or leave a inner switch table.
     * @return node which represent the init of flag variable
     */
    private Node createBreakVarInit(){
        Node varNode = new Node(Token.VAR);
        Node nameNode = Node.newString(Token.NAME, BREAK_VAR_NAME);
        Node trueNode = new Node(Token.TRUE);
        nameNode.addChildToFront(trueNode);
        varNode.addChildToFront(nameNode);
        return varNode;
    }

    /**
     * creates variable init of pc
     * @param startBlockId start assignment of pc variable
     * @return var decl statement
     */
    private Node createPCInit(long startBlockId){
        Node varNode = new Node(Token.VAR);
        Node nameNode = Node.newString(Token.NAME, PC_NAME);
        Node numberNode = Node.newNumber(startBlockId);
        nameNode.addChildToFront(numberNode);
        varNode.addChildToFront(nameNode);
        return varNode;
    }

    /**
     * creates a switch statement
     * @return
     */
    private Node createSwitch() {
        Node switchNode = new Node(Token.SWITCH);
        Node name = Node.newString(Token.NAME, PC_NAME);
        switchNode.addChildToBack(name);
        return switchNode;
    }

    /**
     * creates a while statement
     * @param switchNode body of loop: in this case the switch statement
     * @param condition condition of loop
     * @return
     */
    private Node createWhile(Node switchNode, Node condition){
        Node block = new Node(Token.BLOCK);
        block.addChildToBack(switchNode);
        Node whileNode = new Node(Token.WHILE);
        if(condition == null) {
            condition = new Node(Token.TRUE);
        }
        whileNode.addChildToBack(condition);
        whileNode.addChildToBack(block);
        return whileNode;
    }

}
