package de.fau.cs.util;


import com.google.javascript.rhino.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * singleton class. used for communication between control flow flattening pass and opaque value pass. opaque value processing
 * adds correct transformations to alias array in blocks for flux. adds face transformations to alias array in blocks for fake flux.
 * @author Tobias Gross
 */
public class BlockHolder {
    private List<Node> blockNodesForFlux;
    private List<Node> blockNodesForFakeFlux;

    private BlockHolder() {
        blockNodesForFakeFlux = new ArrayList<>();
        blockNodesForFlux = new ArrayList<>();
    }

    private static BlockHolder that;
    public static BlockHolder getInstance(){
        if(that == null){
            that = new BlockHolder();
        }
        return that;
    }

    public List<Node> getBlockNodesForFlux() {
        return blockNodesForFlux;
    }

    public List<Node> getBlockNodesForFakeFlux() {
        return blockNodesForFakeFlux;
    }

    public boolean addBlockForFlux(Node node) {
        return blockNodesForFlux.add(node);
    }

    public boolean addBlockForFakeFlux(Node node) {
        return blockNodesForFakeFlux.add(node);
    }

    public void clear() {
        blockNodesForFlux.clear();
        blockNodesForFakeFlux.clear();
    }
}
