package de.fau.cs.util;

import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * this name provider creates valid javascript identifier names which looks like hexadecimal numbers. instead of a leading
 * zero a o is used
 * @author Tobias Gross
 */
public class HexNameProvider implements NameProvider {
    private Random random;

    private final static String prefix = "Ox";

    public HexNameProvider(long seed) {
        this.random = new Random(seed);
    }

    @Override
    public String getNextName() {
        String name;
        int rnd = random.nextInt(Integer.MAX_VALUE);
        name = prefix + Integer.toHexString(rnd);
        return name;
    }

    @Override
    public void restoreNames(List<String> names) {

    }
}
