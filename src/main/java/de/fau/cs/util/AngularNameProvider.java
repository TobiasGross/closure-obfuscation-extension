package de.fau.cs.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * this class reads the functionNames file and creates a list of function names. serves a random name from the list.
 * each name is served once.
 * @author Tobias Gross
 */
public class AngularNameProvider implements NameProvider{
    private final static String NAME_PREFIX = "____gen";
    private final static String[] NAME_FILES = {"angular.names"};

    private final List<String> names;
    private final Random random;
    private final Set<String> usedGenericNames;

    /**
     * initialisation with salt. ensure that that names ares served in same order for equivalent salts
     * @param salt
     */
    public AngularNameProvider(long salt) {
        names = new ArrayList<>();
        random = new Random(salt);
        usedGenericNames = new HashSet<>();
        initNames();
    }

    /**
     * reads name file and creates list
     */
    private void initNames(){
        for(String nameFile : NAME_FILES){
            InputStream in = AngularNameProvider.class.getResourceAsStream(nameFile);
            if(in != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                reader.lines().forEach(s -> names.add(s));
            }
        }
    }

    /**
     * serves a unique name. if all names in the list are used generic names are served
     * @return
     */
    public String getNextName(){
        if(names.isEmpty()){
            return generateName(Math.abs(random.nextLong()));
        }else{
            return getNameFromList(Math.abs(random.nextInt()));
        }
    }

    public void restoreNames(List<String> names){
        this.names.addAll(names);
    }

    private String getNameFromList(int index){
        return names.remove(index % names.size());
    }

    /**
     * generates generic name
     * @param index
     * @return
     */
    private String generateName(long index){
        for(int i = 0; true; i++){
            if(usedGenericNames.add(NAME_PREFIX + (index + i))){
                return NAME_PREFIX + index;
            }
        }
    }
}
