package de.fau.cs.util;

import java.util.List;

/**
 * @author Tobias Gross
 */
public interface NameProvider {

    /**
     * serves a unique name. if all names in the list are used generic names are served
     * @return
     */
    public String getNextName();

    /**
     * should return the given names in the backlog of names
     * @param names
     */
    public void restoreNames(List<String> names);
}
