package de.fau.cs.util;

import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * this nameprovider should only be used for temporary names. generates names with leading __rnd and a random number
 * afterwards
 * @author Tobias Gross
 */
public class RandomNameProvider implements NameProvider {
    private Random random;
    Set<String> excludedNames;
    private final String postfix;

    private final static String prefix = "__rnd";

    public RandomNameProvider(long seed, Set<String> excludedNames, String postfix) {
        this.random = new Random(seed);
        this.excludedNames = excludedNames;
        this.postfix = postfix;
    }

    @Override
    public String getNextName() {
        String name;
        do{
            int rnd = random.nextInt(Integer.MAX_VALUE);
            name = prefix + rnd + postfix;
        }while(excludedNames.contains(name));
        return name;
    }

    @Override
    public void restoreNames(List<String> names) {

    }
}
