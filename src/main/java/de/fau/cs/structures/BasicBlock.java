package de.fau.cs.structures;

import com.google.javascript.jscomp.CodePrinter;
import com.google.javascript.rhino.Node;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Objects of this class build a basic block graph. Each basic block (bb) consists of statement(s), which are consecutively executed.
 * at the end of a bb is a jump, which points to another bbs. the jump can be conditional or unconditional. in case of unconditional jump
 * condition attribute is not set. JMP attribute points to next bb.
 * in case of conditional jump a condition is set. this will point to a AST part which evaluates to boolean. trueJMP and falseJMP directs to bbs
 * in each case. each bb is identified by a id, that has to be unique in scope of a function. id is used for switch case transformation as case id.
 * @author Tobias Gross
 */
public class BasicBlock {
    private final long id;
    private final List<Node> statements;
    private Node condition;

    private BasicBlock JMP = null;
    private BasicBlock trueJMP = null;
    private BasicBlock falseJMP = null;

    public BasicBlock(long id) {
        this.id = id;
        statements = new ArrayList<>();
    }

    public boolean addStatement(Node node) {
        return statements.add(node);
    }

    public long getId() {
        return id;
    }

    public List<Node> getStatements() {
        return statements;
    }

    public BasicBlock getJMP() {
        return JMP;
    }

    public void setJMP(BasicBlock JMP) {
        assert(trueJMP == null && falseJMP == null && condition == null);
        this.JMP = JMP;
    }

    public BasicBlock getTrueJMP() {
        return trueJMP;
    }

    public void setTrueJMP(BasicBlock trueJMP) {
        assert(JMP == null);
        this.trueJMP = trueJMP;
    }

    public BasicBlock getFalseJMP() {
        return falseJMP;
    }

    public void setFalseJMP(BasicBlock falseJMP) {
        assert(JMP == null);
        this.falseJMP = falseJMP;
    }

    public Node getCondition() {
        return condition;
    }

    public void setCondition(Node condition) {
        assert(JMP == null);
        this.condition = condition;
    }

    /**
     * All attributes of that are copied to this basic block, except id;
     * @param that another basic block
     */
    public void copyValues(BasicBlock that){
        this.statements.clear();
        this.statements.addAll(that.statements);
        this.condition = that.condition;
        this.JMP = that.JMP;
        this.trueJMP = that.trueJMP;
        this.falseJMP = that.falseJMP;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BasicBlock)) return false;

        BasicBlock that = (BasicBlock) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
