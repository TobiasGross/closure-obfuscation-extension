package de.fau.cs.structures;

import com.google.javascript.rhino.Node;
import de.fau.cs.util.AngularNameProvider;
import de.fau.cs.util.NameProvider;

import java.util.*;
import java.util.stream.Collectors;


/**
 * this class holds all declared names (functions, parameter, variables) of a function scope
 * @author Tobias Gross
 */
public class Scope {
    private Scope parent;
    private List<Scope> children = new ArrayList<>();

    private int nextEnterCandidate = 0;

    private Node function;

    private Map<String, Identifier> declaredNames = new HashMap<>();

    public Scope(Scope parent, Node function) {
        this.parent = parent;
        this.function = function;
    }

    /**
     * called by enter method in first traversal. builds up the scope structure.
     * @param child
     */
    public void addChild(Scope child){
        children.add(child);
    }

    /**
     * enters correct child scope in second traversal.
     * @return new active scope
     */
    public Scope enter(){
        Scope ret = children.get(nextEnterCandidate);
        nextEnterCandidate++;
        return ret;
    }

    /**
     * leaves scope and returns parent scope. used in building a scope tree.
     * @return parent scope
     */
    public Scope leave(){
        return parent;
    }

    /**
     * adds a function declaration to this scope.
     * @param decl a name node
     */
    public void addFuncDecleration(Node decl){
        addDecl(decl, Identifier.IdentifierType.FUNCTION);
    }

    /**
     * adds a variable declaration to this scope.
     * @param decl a name node
     */
    public void addVarDecleration(Node decl){
        addDecl(decl, Identifier.IdentifierType.VARIABLE);
    }

    /**
     * adds a parameter declaration to this scope.
     * @param decl a name node
     */
    public void addParamDecleration(Node decl){
        addDecl(decl, Identifier.IdentifierType.PARAMETER);
    }

    /**
     *
     * @return all scopes opened under this scope
     */
    public List<Scope> getChildren() {
        return children;
    }

    /**
     * adds a declaration to this scope
     * @param decl a node with type name
     * @param type variable, function or parameter
     */
    private void addDecl(Node decl, Identifier.IdentifierType type){
        String name = decl.getString();
        if(!declaredNames.containsKey(name)) {
            Identifier ident = new Identifier(decl, type);
            declaredNames.put(name, ident);
            //todo find better solution for multiple var decls in same scope
        }else {
            Identifier ident = declaredNames.get(name);
            ident.addCall(decl);
        }
    }


    /**
     * this method is used in the second ast traversal to register references to their matching declarations
     * @param ref a name node
     */
    public void addNameRef(Node ref){
        Scope iterator = this;

        while(iterator != null){
            Identifier ident = iterator.declaredNames.get(ref.getString());
            if(ident != null){
                ident.addCall(ref);
                break;
            }
            iterator = iterator.parent;
        }
    }

    /**
     * this method checks if this scope contains a declaration like the name parameter
     * @param name
     * @return
     */
    public boolean hasDecleration(Node name){
        return declaredNames.containsKey(name.getString());
    }

    /**
     * This function returns the identifier matching to given name. will traverse the scope tree up to the root scope.
     * A identifier holds the AST node of decleration and all references to this decleration.
     * @param name
     * @return
     */
    public Identifier getDecleration(Node name){
        if(declaredNames.containsKey(name.getString())){
            return declaredNames.get(name.getString());
        }
        if(parent == null){
            return null;
        }
        return parent.getDecleration(name);
    }

    /**
     * returned identifier are ready to rename and won't be changed or extended by the ast traversal.
     * in second traversal, this method should be called before leave.
     * @return all identifier of this scope
     */
    public Collection<Identifier> prepareLeave(){
        return declaredNames.values();
    }

    /**
     * this method checks if a given name is already defined in this scope or a parent scope
     * @param newName
     * @return
     */
    public boolean nameCollide(String newName){
        if(parent != null){
            if(parent.nameCollide(newName)){
                return true;
            }
        }
        if(declaredNames.containsKey(newName)){
            return true;
        }
        return false;
    }

    /**
     * returns all identifiers of a given type which are declared in this scope or a parent scope
     * @param type
     * @return
     */
    private List<Identifier> getAllIdentifiers(Identifier.IdentifierType type){
        List<Identifier> identifiers = getIdentifiers(type);
        for(Scope s : children){
            identifiers.addAll(s.getAllIdentifiers(type));
        }
        return identifiers;
    }

    /**
     * returns all function identifiers declared in this scope or a parent scope
     * @return
     */
    public List<Identifier> getAllFunctionIdentifiers(){
        return getAllIdentifiers(Identifier.IdentifierType.FUNCTION);
    }

    /**
     * returns all variable identifiers declared in this scope or a parent scope
     * @return
     */
    public List<Identifier> getAllVariableIdentifiers(){
        return getAllIdentifiers(Identifier.IdentifierType.VARIABLE);
    }

    /**
     * returns all parameter identifiers declared in this scope or a parent scope
     * @return
     */
    public List<Identifier> getAllParameterIdentifiers(){
        return getAllIdentifiers(Identifier.IdentifierType.PARAMETER);
    }

    /**
     * returns all funtion identifiers declared in this scope
     * @return
     */
    public List<Identifier> getFunctionIdentifiers(){
        return getIdentifiers(Identifier.IdentifierType.FUNCTION);
    }

    /**
     * returns all identifiers declared in this scope
     * @return
     */
    public List<Identifier> getIdentifiers(){
        return getIdentifiers(null);
    }

    /**
     * return all identifiers in this scope. filters identifiers by the given type
     * @param type
     * @return
     */
    private List<Identifier> getIdentifiers(Identifier.IdentifierType type){
        if(type == null){

            return new ArrayList<>(declaredNames.values());
        }
        return declaredNames.values().stream().filter(identifier -> identifier.is(type)).collect(Collectors.toList());
    }

    /**
     * returns a name from given name provider, which doas not collide with declared names in current scope chain
     * @param nameProvider
     * @return
     */
    private String getNameFromProvider(NameProvider nameProvider){
        List<String> collideNames = new ArrayList<>();
        String newName = nameProvider.getNextName();
        while(nameCollide(newName)){
            collideNames.add(newName);
            newName = nameProvider.getNextName();
        }
        nameProvider.restoreNames(collideNames);
        return newName;
    }

    /**
     * renames identifiers given types with names from name provider
     * @param nameProvider
     * @param type
     */
    private void rename(NameProvider nameProvider, Identifier.IdentifierType type){
        for(Identifier ident : declaredNames.values()){
            if(ident.is(type)){
                String newName = getNameFromProvider(nameProvider);
                ident.rename(newName);
            }
        }
    }

    /**
     * renames functions with names from name provider
     * @param nameProvider
     */
    public void renameFunctions(NameProvider nameProvider){
        rename(nameProvider, Identifier.IdentifierType.FUNCTION);

        for(Scope s : children){
            s.renameFunctions(nameProvider);
        }
    }

    /**
     * renames variables with names from name provider
     * @param nameProvider
     */
    public void renameVariables(NameProvider nameProvider){
        rename(nameProvider, Identifier.IdentifierType.VARIABLE);

        for(Scope s : children){
            s.renameVariables(nameProvider);
        }
    }

    /**
     * renames parameter with names from name provider
     * @param nameProvider
     */
    public void renameParameter(NameProvider nameProvider){
        rename(nameProvider, Identifier.IdentifierType.PARAMETER);

        for(Scope s : children){
            s.renameParameter(nameProvider);
        }
    }

    /**
     * traverse scope tree and returns scope object which is related to given function node
     * @param function
     * @return
     */
    public Scope getFunctionScope(Node function){
        if(this.function != null && this.function.equals(function)){
            return this;
        }else{
            for(Scope s : children){
                Scope retVal = s.getFunctionScope(function);
                if(retVal != null){
                    return retVal;
                }
            }
            return null;
        }
    }

    /**
     * return related function node
     * @return
     */
    public Node getFunction() {
        return function;
    }
}
