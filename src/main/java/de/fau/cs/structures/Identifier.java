package de.fau.cs.structures;

import com.google.javascript.rhino.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * Objects of this class contains a declaration with there corresponding references. They are divided to function, parameter and variable declarations.
 * @author Tobias Gross
 */
public class Identifier {
    private Node decl;
    private List<Node> calls;
    private IdentifierType type;

    Identifier(Node decl) {
        this.decl = decl;
        calls = new ArrayList<>();
        type = IdentifierType.FUNCTION;
    }

    Identifier(Node decl, IdentifierType type) {
        this.decl = decl;
        calls = new ArrayList<>();
        this.type = type;
    }

    boolean addCall(Node node) {
        return calls.add(node);
    }

    public void rename(String newName){
        decl.setString(newName);
        calls.forEach((Node n) -> {
            n.setString(newName);
        });
    }

    public boolean isFuncIdentifier() {
        return type.equals(IdentifierType.FUNCTION);
    }

    public boolean isVarIdentifier(){
        return type.equals(IdentifierType.VARIABLE);
    }

    public boolean isParamIdentifer(){
        return type.equals(IdentifierType.PARAMETER);
    }

    public boolean is(IdentifierType type){
        return this.type.equals(type);
    }

    public int compare(Identifier that){
        if(this.decl.getLineno() == that.decl.getLineno()){
            return this.decl.getString().compareTo(that.decl.getString());
        }
        if(this.decl.getLineno() > that.decl.getLineno()){
            return 1;
        }else{
            return -1;
        }
    }

    public enum IdentifierType {
        FUNCTION,
        PARAMETER,
        VARIABLE
    }

    public Node getDecl() {
        return decl;
    }

    public List<Node> getRefs() {
        return calls;
    }
}
