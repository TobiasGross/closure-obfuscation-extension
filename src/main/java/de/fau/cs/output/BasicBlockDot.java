package de.fau.cs.output;

import com.google.javascript.jscomp.CodePrinter;
import com.google.javascript.rhino.Node;
import de.fau.cs.structures.BasicBlock;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is used to print a basic block graph in graphviz dot format. It handles basic block objects and creates graphviz nodes.
 * Each node consist of statements which will be plotted in js syntax. Jumps between basic blocks will be plotted as graphviz edges
 * (black for unconditional, green for true and red for false jump).
 * @author Tobias Gross
 */
public class BasicBlockDot {
    private final static String FUNCTION_REGEX = "function\\([a-zA-Z, ]*\\)\\{";
    private final static Pattern FUNCTION_PATTERN;
    private final static String NODE_LABEL_START = " [label=\"";
    private final static String NODE_LABEL_END = "\"];";
    private final static String LABEL_FOR_NO_STATEMENTS = "NOP";
    private final static String NODE_STRUCT_SEPERATOR = " | ";

    private final static String STRUCT_END_ELEMENT_TOKEN = "end";
    private final static String STRUCT_START_ELEMENT_TOKEN = "start";
    private final static String STRUCT_END_ELEMENT_TOKEN_DEF = "<" + STRUCT_END_ELEMENT_TOKEN + ">";
    private final static String STRUCT_START_ELEMENT_TOKEN_DEF = "<" + STRUCT_START_ELEMENT_TOKEN + ">";
    private final static String START_ELEMENT_REF = ":" + STRUCT_START_ELEMENT_TOKEN;
    private final static String END_ELEMENT_REF = ":" + STRUCT_END_ELEMENT_TOKEN;

    private final static String NODE_PREFIX = "struct";
    private final static String EDGE_TOKEN = " -> ";
    private final static String EDGE_GREEN = "[color=\"green\"]";
    private final static String EDGE_RED = "[color=\"red\"]";

    private final static String[] CHARS_TO_ESCAPE = {"\"", "<", ">", "{", "}"};
    private final static String ESCAPE_CHAR = "\\";

    static{
        FUNCTION_PATTERN = Pattern.compile(FUNCTION_REGEX);
    }

    /**
     * @param stream plot target
     * @param prefix this prefix will be added to basic block identifier. can be used to differentiate different basic blocks with same id
     * @param basicBlock a node is plotet for this basic block. for all jumps a edge is plotted
     */
    public static void printBasicBlock(PrintStream stream, int prefix, BasicBlock basicBlock){
        int countStatements = 0;
        countStatements += basicBlock.getStatements().size();
        countStatements += basicBlock.getCondition() == null ? 0 : 1;
        printNode(stream, prefix, basicBlock, countStatements);
        printEdges(stream, prefix, basicBlock, countStatements);
    }

    /**
     * plots a node for basic block
     * @param stream
     * @param prefix
     * @param basicBlock
     * @param countStatements used to determine if start and end statement of basic block is equal
     */
    private static void printNode(PrintStream stream, int prefix, BasicBlock basicBlock, int countStatements){
        StringBuilder sb = new StringBuilder();

        sb.append(getDotId(Integer.toString(prefix), basicBlock));
        sb.append(NODE_LABEL_START);
        sb.append(STRUCT_START_ELEMENT_TOKEN_DEF);
        sb.append(" ");

        if(basicBlock.getCondition() == null && basicBlock.getStatements().isEmpty()){
            sb.append(LABEL_FOR_NO_STATEMENTS);
            sb.append(NODE_LABEL_END);
            stream.println(sb.toString());
            return;
        }

        List<Node> statements = basicBlock.getStatements();


        if(basicBlock.getCondition() == null){
            statements = new ArrayList<>(basicBlock.getStatements());
            statements.remove(statements.size() - 1);
        }

        for(Node statement : statements){
            sb.append(getCode(statement));
            sb.append(NODE_STRUCT_SEPERATOR);
        }

        if(countStatements > 1) {
            sb.append(STRUCT_END_ELEMENT_TOKEN_DEF);
            sb.append(" ");
        }
        if(basicBlock.getCondition() == null){
            sb.append(getCode(basicBlock.getStatements().get(basicBlock.getStatements().size() - 1)));
        }else{
            sb.append(getCode(basicBlock.getCondition()));
        }
        sb.append(NODE_LABEL_END);
        //e.g.: struct00 [label="<start> var a=0 | a=a+5 | <end> a\<3"];
        stream.println(sb.toString());
    }

    /**
     * plots edges for jumps of basic block
     * @param stream
     * @param prefix
     * @param basicBlock
     * @param countStatements used to determine if start and end statement of basic block is equal
     */
    private static void printEdges(PrintStream stream, int prefix, BasicBlock basicBlock, int countStatements){
        String nodeStart = END_ELEMENT_REF;

        if(countStatements <= 1){
            nodeStart = START_ELEMENT_REF;
        }
        StringBuilder sb = new StringBuilder();
        if(basicBlock.getJMP() != null) {
            //results in e.g.: struct01 -> struct 02;
            sb.append(getDotId(Integer.toString(prefix), basicBlock));
            sb.append(nodeStart);
            sb.append(EDGE_TOKEN);
            sb.append(getDotId(Integer.toString(prefix), basicBlock.getJMP()));
            sb.append(START_ELEMENT_REF);
            sb.append(";");
            stream.println(sb.toString());
        }else if (basicBlock.getTrueJMP() != null){
            //results in e.g.: struct01-> struct02 [color="green"];
            sb.append(getDotId(Integer.toString(prefix), basicBlock));
            sb.append(nodeStart);
            sb.append(EDGE_TOKEN);
            sb.append(getDotId(Integer.toString(prefix), basicBlock.getTrueJMP()));
            sb.append(START_ELEMENT_REF);
            sb.append(" " + EDGE_GREEN + ";");
            stream.println(sb.toString());
            //results in e.g.: struct01-> struct03 [color="red"];
            sb = new StringBuilder();
            sb.append(getDotId(Integer.toString(prefix), basicBlock));
            sb.append(nodeStart);
            sb.append(EDGE_TOKEN);
            sb.append(getDotId(Integer.toString(prefix), basicBlock.getFalseJMP()));
            sb.append(START_ELEMENT_REF);
            sb.append(" " + EDGE_RED + ";");
            stream.println(sb.toString());
        }
    }

    /**
     * @param prefix
     * @param basicBlock
     * @return dot node name
     */
    private static String getDotId(String prefix, BasicBlock basicBlock){
        return NODE_PREFIX + prefix + basicBlock.getId();
    }

    /**
     * this method generates js code of AST parts. Uses closures CodePrinter.Builder class. AST parts which are function
     * definitions are shortened. Special dot characters are escaped.
     * @param node
     * @return
     */
    private static String getCode(Node node){
        CodePrinter.Builder builder = new CodePrinter.Builder(node);
        String code = builder.build();
        code = shortenFunctionDef(code);
        code = escapeDotChars(code);
        return code;
    }

    private static String escapeDotChars(String input){
        for(String character : CHARS_TO_ESCAPE){
            input = input.replace(character, ESCAPE_CHAR + character);
        }
        return input;
    }

    /**
     * a function def statement will be shorten to a notation without function body
     * @param input full function definition in javascript syntax
     * @return short version of function definition
     */
    private static String shortenFunctionDef(String input){
        Matcher m = FUNCTION_PATTERN.matcher(input);
        if(m.find()) {
            int index = m.end();
            input = input.substring(0, index);
            input = input + " ... }";
        }
        return input;
    }
}
